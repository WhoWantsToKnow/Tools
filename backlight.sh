#!/bin/bash
# ==================================================================================== #
#         FILE: backlight.sh                                                           #
# ==================================================================================== #
#  DESCRIPTION: Script to save or restore the current brightness level to a file in    #
#               /etc. It is intended to be invoked via a systemd service call on       #
#               session start and shutdown.                                            #
#                                                                                      #
#               NB must be run with root privileges e.g. use sudo                      #
#                                                                                      #
# ==================================================================================== #
#        USAGE: backlight load|save                                                    #
#        NEEDS: xbacklight                                                             #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.0.2                                                                  #
# DATE CREATED: 20-Nov-2018 22:16                                                      #
# LAST CHANGED: 15-May-2020 19:52
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnEcho () {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ------------------------------------------------------------------------------------ #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  SCRIPTNAME="${0##*/}"          # base name of script
declare -r  BACKLIGHT="/etc/backlight"
declare -r  XBACKLIGHT="/usr/bin/xbacklight"
declare -r  COMMAND=${1,,}                 # force argument 1 to lowercase
# --------------------------------- error messages ----------------------------------- #
declare -r ERM1="FATAL ERROR: must be run with root privileges.\n"
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i Result=${TRUE}
declare -i Backlight
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
fnEcho "${NL}"
if [[ $USER != 'root' ]]; then
    fnEcho "${ERM1}"
    Result=${FALSE}
else
    if [[ "${COMMAND}" = "load" ]]; then
        Backlight=$(cat ${BACKLIGHT:-50})
        ${XBACKLIGHT} -set ${Backlight}
        fnEcho "backlight set to ${Backlight}\n"
    fi
    if [[ "${COMMAND}" = "save" ]]; then
        Backlight=$(${XBACKLIGHT} -get)
        echo ${Backlight} > "${BACKLIGHT}"
        fnEcho "backlight saved as ${Backlight}\n"
    fi
fi
exit ${Result}
