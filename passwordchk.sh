#!/bin/bash
# ==================================================================================== #
#         FILE: passwordchk.sh                                                         #
# ==================================================================================== #
#  DESCRIPTION: use the 'chage' command to get a report on password aging and notify   #
#               the user if the password is within the warning period of expiry.       #
#                                                                                      #
# ==================================================================================== #
#        USAGE: passwordchk                                                            #
#        NEEDS: chage, notify-send                                                     #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 2.1.2                                                                  #
# DATE CREATED: not recorded                                                           #
# LAST CHANGED: 04-Jun-2020 00:51                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnGetPWInfo() {
# ------------------------------------------------------------------------------------ #
# Parses the output from the chage command to get the password expiry date and days of #
# warning value.                                                                       #
# Arguments: none                                                                      #
# Returns: sets global variables.                                                      #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # Set up some simple regular expressions for use when searching command output for
    # strings with embedded spaces.
    declare -r RE1="^Password expires"
    declare -r RE2="Number of days"
    # constants used to set and reset the IFS
    declare -r NEWLINE=$'\n'
    declare -r COLON=":"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # set field separator to newline only
    IFS=${NEWLINE}
    # iterate over user account aging information report from 'chage' command
    for LINE in $(sudo chage -l ${USER}); do
        # split lines at colon character
        IFS=${COLON}
        # look for line of form "Password expires                 : MMMfMM DD, YYYY"
        if [[ ${LINE} =~ ${RE1} ]]; then
            # get expiry date string
            for WORD in ${LINE}; do
                EXPDATE=${WORD}
            done
        # line of form "Number of days of warning before password expires    : D"
        elif [[ ${LINE} =~ ${RE2} ]]; then
            # get number of days of warning before password expires
            for WORD in ${LINE}; do
                WARNDAYS=${WORD}
            done
        fi
        # set field separator back to newline
        IFS=${NEWLINE}
    done
}
# ------------------------------------------------------------------------------------ #
function fnNotify() {
# ------------------------------------------------------------------------------------ #
# Uses notify-send to display appropriate messages to let the user know that the       #
# password has either expired or is near to expiring.                                  #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # constants used with the notify send
    declare -r APPLICATION="Password Check"
    declare -r TITLE="PLEASE NOTE!!"
    declare -r ICON="dialog-warning"
    declare -r DELAY=1.51
    declare -r TIME=1500
    declare -r TEXT1="Password has expired.\nMust be changed NOW!!"
    declare -r TEXT2="Password expires today.\nPlease change ASAP."
    declare -r TEXT3="Password will expire in %s days."
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # has password expired?
    if [[ ${EXPDAYS} -lt 0 ]]; then
        printf -v TEXT "${TEXT1}"
    # is password expiring today?
    elif [[ ${EXPDAYS} -lt 1 ]]; then
        printf -v TEXT "${TEXT2}"
    else
        # password expiring in n days
        printf -v TEXT "${TEXT3}" "${EXPDAYS}"
    fi
    # display the notifications 3 times
    for COUNT in {1..3}; do
        notify-send -t ${TIME}          \
                    -a "${APPLICATION}" \
                    "${TITLE}"          \
                    "${TEXT}"           \
                    --icon="${ICON}"
        sleep ${DELAY}
    done
    notify-send -a "${APPLICATION}" "${TITLE}" "${TEXT}" --icon="${ICON}"
}
# ------------------------------------------------------------------------------------ #
#                                                                                      #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
#                                                                                      #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
#                                                                                      #
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
#                                                                                      #
declare    EXPDATE=""              # expiry date string extracted from command output
declare    WARNDAYS=""             # number of days of warning before password expires
declare    EXPDAYS=""              # number of days until password expires
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#                                                                                      #
# populate expiry date and number of warning days
fnGetPWInfo
#
# if password never expires then exit
if [[ ${EXPDATE} =~ never ]]; then
    exit 0
fi
#
# calculate number of days until password expires (expiry date - todays date)
EXPDAYS=$(( ($(date -ud ${EXPDATE} +%s)-$(date -u +%s))/60/60/24 ))
#
# if within warning period show repeated notifications.
if [[ ${EXPDAYS} -lt ${WARNDAYS} ]]; then
    fnNotify
fi
