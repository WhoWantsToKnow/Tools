#!/bin/sh
find /usr/share/icons -mindepth 1 -maxdepth 1 -type d | while read -r THEME; do
  if [ -f "$THEME/index.theme" ]; then
    echo $THEME
    gtk-update-icon-cache -f "$THEME"
  fi
done
