#!/bin/bash
# ==================================================================================== #
#         FILE: whereused.sh                                                           #
# ==================================================================================== #
#  DESCRIPTION: Will process a bash source file to produce a list of functions and     #
#               variables and a cross reference report of where they occur within the  #
#               source.                                                                #
#                                                                                      #
#               NB the logic presumes that function and variable declarations follow   #
#                  my usual coding style (i.e. as exemplified by this source file).    #
# ==================================================================================== #
#        USAGE: ./whereused.sh [bash source file]                                      #
#        NEEDS: ---                                                                    #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.0.0                                                                  #
# DATE CREATED: 17-Aug-2024                                                            #
# LAST CHANGED: 24-Nov-2024 10:27                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
# set extended pattern matching on so that it's enabled BEFORE bash starts any syntax
# checking. NB '*([[:blank:]])' matches leading spaces and tabs, needs 'extglob' enabled
shopt -s extglob
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnMain(){
# ------------------------------------------------------------------------------------ #
# holds the primary processing logic for the script.                                   #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r MAINCODE="*Primary*Code*"  # indicate not in function definition
    declare -r STARTSCAN="Where Used Report For : %s - SLOC Count : %05d %s\n"
    declare -r ENDSCAN="Found %d function definitions and %d variable declarations.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -- StartScan                # program start header message
    declare -i Item=0                   # general work index variable
    declare -i LineNo=0                 # work variable to hold line number
    declare -i MaxFVLen=0               # maximum length of function/variable name
    declare    FName=""                 # current function name
    declare -i MaxFNLen=0               # maximum function name length so far
    declare    VName=""                 # current variable name
    declare -i MaxVNLen=0               # maximum variable name length so far
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get script source into array
    readarray -t ScriptSource < "${SCRIPTSOURCE}"
    NumLines=${#ScriptSource[@]}        # count number of source lines
    # remove all comment text so there are no false positives when variable
    # and function names are mentioned.
    for (( Item=0; Item<NumLines; Item++ )); do
        ScriptSource[${Item}]="${ScriptSource[${Item}]/\#*/\#}"
    done
    # display an initial run message
    StartScan=$(printf "${STARTSCAN}" "${SOURCENAME}" ${NumLines} \
                                                "$(date "+on %A %e %B %Y at %I:%M%p")")
    fnMsg "${BOTH}" "%s\n" "${StartScan}"
    fnMsg "${BOTH}" "%s\n" $(fnCopies "=" ${#StartScan})
    fnEcho "\n"
    MaxFNLen=0                          # maximum function name length
    MaxVNLen=0                          # maximum variable name length
    fnGetFunctions                      # get function data
    fnGetVariables                      # get variable data
    # get the larger of the maximum function and variable name lengths
    MaxFVLen=$((MaxFNLen > MaxVNLen ? MaxFNLen : MaxVNLen))
    # display message to say finished looking for functions and variables
    fnMsg "${STDERR}" "${ENDSCAN}" ${NoFunctions} ${NoVariables}
    fnPrintReport                       # output final report
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetFunctions(){
# ------------------------------------------------------------------------------------ #
# Scan source code for function beginning and end lines and build a dictionary indexed #
# by function name and taking a value of start line no. concatenated with end line     #
# number. Scan source code for function references and build a dictionary, indexed by  #
# function name, of functions called by each function then build a dictionary of       #
# functions that call each function, also indexed by function name.                    #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # regex search strings for use when scanning source code
    # function definition start e.g. function xx..xx(){
    declare -r  FNSTART="^[[:blank:]]*function[[:blank:]]*[A-Za-z][A-Za-z0-9_][^(]*"
    declare -r  FNEND="^\}"             # find end of  function definitions
    # find function references within code
    declare -r  FNREF="fn[A-Za-z0-9]*"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # -- Function definition processing
    declare -a FunctionStart            # list of function definition lines
    declare -a FunctionEnd              # list of function end lines
    declare -a FunctionRef              # list of function references in code
    declare -i FunCount=0
    declare -a Refs
    declare -a Calls
    declare    RefFN                    # function name relating to a specific line no.
    declare    FName                    # function name holding item
    declare    LineNo                   # function start/end source line number
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get function definitions from source into array: 9999:[function name]
    readarray -t FunctionStart < <(${GREP} -no ${FNSTART} \
                                                < <(printf "%s\n" "${ScriptSource[@]}"))
    FunctionStart=("${FunctionStart[@]/function /}") # delete 'function ' from each item
    # get function end lines from source into array: 9999
    readarray -t FunctionEnd < <(${GREP} -no ${FNEND} \
                                                < <(printf "%s\n" "${ScriptSource[@]}"))
    FunctionEnd=("${FunctionEnd[@]/:\}/}")     # delete ':\}' from each item
    # add entries to function dictionary of form [start line no.]:[end line no.]
    # dictionary indexed by function name.
    for (( Item=0; Item<${#FunctionStart[@]}; Item++ )); do
        LineNo=${FunctionStart[${Item}]%:*}    # extract start line number
        FName="${FunctionStart[${Item}]#*:}"   # extract function name
        # update maximum function name length if required
        MaxFNLen=$(( ${#FName} > MaxFNLen ? ${#FName} : MaxFNLen ))
        Function[${FName}]="${LineNo}"         # add start line number
        # append end line number to value
        Function[${FName}]+=":${FunctionEnd[${Item}]}"
        # note largest end line number to identify start of primary code block
        PrimaryStart=$(( ${FunctionEnd[${Item}]} > PrimaryStart ? \
                                               ${FunctionEnd[${Item}]} : PrimaryStart ))
    done
    NoFunctions=${#Function[@]}
    # get list of function references within the source : 9999:[function name]
    readarray -t FunctionRef < <(${GREP} -no ${FNREF} \
                                                < <(printf "%s\n" "${ScriptSource[@]}"))
    FunctionRef=("${FunctionRef[@]// /}") # delete spaces from each item
    for FName in "${!Function[@]}"; do  # process each function in turn
        FunCount=$((FunCount+1))
        fnProgress ${FunCount} ${NoFunctions} "Scanning Functions"
        Start=${Function[${FName}]%:*}  # get 1st line no. of function
        End=${Function[${FName}]#*:}    # get last line no. of function
        Refs=(${Refs[@]:1:0})           # clear down working array
        fnGetFunctionCalls      # build dictionary of functions which call this function
        fnGetFunctionsCalled    # build dictionary of functions called by each function
    done
    # process primary code block for function references
    FName="${MAINCODE}"
    (( PrimaryStart++ ))            # bump past last line of last function definition
    Start=${PrimaryStart}           # get 1st line no. of primary code block
    End=${NumLines}                 # get last line no. of primary code block
    Refs=(${Refs[@]:1:0})           # clear down working array
    # build list of functions called from primary code block
    for (( Item=0; Item<${#FunctionRef[@]}; Item++ )); do
        # skip if same as function being processed
        if [[ ${FunctionRef[${Item}]} == *"${FName}"*  ]]; then
            continue
        fi
        # skip until function start line reached
        if (( ${FunctionRef[${Item}]%:*} < Start )); then
            continue
        fi
        # end when function end line reached
        if (( ${FunctionRef[${Item}]%:*} > End )); then
            break
        fi
        # add called function name to working array
        Refs+=(" ${FunctionRef[${Item}]#*:}")
    done
    # sort working array items eliminating duplicates
    readarray -td '' Calls < <(printf '%s\0' "${Refs[@]}" | sort -z | uniq -z)
    # add sorted list to dictionary of functions called
    FunCalls[${FName}]="${Calls[@]}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetFunctionCalls(){
# ------------------------------------------------------------------------------------ #
# Build dictionary of functions which call this function.                              #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # NB all constants and variables are declared in calling function
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Refs=(${Refs[@]:1:0})           # clear down working array
    # iterate over function references to get called by functions
    # function reference is 9999:[function name]
    for (( Item=0; Item<${#FunctionRef[@]}; Item++ )); do
        # test for entries for this function
        if [[ "${FunctionRef[${Item}]#*:}" == "${FName}" ]]; then
            # get the line number
            LineNo=${FunctionRef[${Item}]%:*}
            # lookup function it relates to
            RefFN="$(fnGetFunctionByLineNo ${LineNo})"
            # only if it isn't within the current base function
            if [[ "${RefFN}" != "${FName}" ]]; then
                # append function reference to working array
                Refs+=(" $(fnGetFunctionByLineNo ${LineNo})")
            fi
        fi
    done
    # sort working array items eliminating duplicates
    readarray -td '' Calls < <(printf '%s\0' "${Refs[@]}" | sort -z | uniq -z)
    # add sorted list to dictionary of called by functions
    FunCalled[${FName}]="${Calls[@]}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetFunctionsCalled(){
# ------------------------------------------------------------------------------------ #
# Build dictionary of functions called by each function                                #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # NB all constants and variables are declared in calling function
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Refs=(${Refs[@]:1:0})           # clear down working array
    # function reference is 9999:[function name]
    for (( Item=0; Item<${#FunctionRef[@]}; Item++ )); do
        # get the function name referred to
        RefFN="${FunctionRef[${Item}]#*:}"
        # skip if same as function being processed
        if [[ "${RefFN}" == "${FName}"  ]]; then
            continue
        fi
        # skip until function start line reached
        if (( ${FunctionRef[${Item}]%:*} < Start )); then
            continue
        fi
        # end when function end line reached
        if (( ${FunctionRef[${Item}]%:*} > End )); then
            break
        fi
        # test if this refers to a known function definition
        if [[ ! ${!Function[@]} =~ ${RefFN} ]]; then
            RefFN="${RefFN}(undefined)" # indicate that this is undefined
        fi
        # add called function name to working array
        Refs+=(" ${RefFN}")
    done
    # sort working array items eliminating duplicates
    readarray -td '' Calls < <(printf '%s\0' "${Refs[@]}" | sort -z | uniq -z)
    # add sorted list to dictionary of functions called
    FunCalls[${FName}]="${Calls[@]}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetVariables(){
# ------------------------------------------------------------------------------------ #
# Scan source code identifying variable declarations and building dictionaries that    #
# hold the details for each declaration. Invokes functions to build lists of           #
# undeclared variables and where the variables are referenced within the source code.  #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # regex search strings for use when scanning source code
    # -------------------------------------------------------------------------------- #
    declare -r  VARDEC="^[[:blank:]]*declare"   # get variable declarations
    declare -r  VARASS="^[[:blank:]]*[A-Za-z0-9_][A-Za-z0-9_]+[=]"
                                                # get variable assignments
    declare -r  INTEGER="^[0-9]+$"              # identify pure integers
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a VariableDecl             # array of lines with variable declarations
    declare -a Token                    # work array for splitting source items
    declare -- Line=""                  # holding variable for source items
    declare -a WorkingList              # used when splitting strings with variables
    declare -A VarAssign                # dictionary of lines where variable assigned
    declare -- VName=""
    declare -i LineNo
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get variable declaration lines from source into array
    # -------------------------------------------------------------------------------- #
    readarray -t VariableDecl < <(${GREP} -n ${VARDEC} \
                                                < <(printf "%s\n" "${ScriptSource[@]}"))
    VariableDecl=("${VariableDecl[@]/\#*/}")    # delete any comments from each line
    for Line in "${VariableDecl[@]}"; do        # get each line into a variable
        IFS=': ' read -r -a Token <<< "${Line}" # split line into parts
        fnGetVariableDetails                    # get variable data into dictionaries
    done
    NoVariables=${#Variable[@]}                 # save number of variable declarations
    # save number of unique variable declarations
    NoUniqVars=$(sort < <(printf "%s\n" "${Variable[@]}") | uniq | wc -l)
    fnFindUndeclared                            # find any undeclared variables
    # get list of variable references in source code
    fnGetVarUsage
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnFindUndeclared(){
# ------------------------------------------------------------------------------------ #
# Scan through source looking for variable assignments and then check that the         #
# assigned variable has previously been declared via a 'declare' statement. If not     #
# add it to a list of undeclared variable names for the final report.                  #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # NB all constants and variables are declared in calling function
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    readarray -t WorkingList < <(${GREP} -Eno ${VARASS} \
                                                < <(printf "%s\n" "${ScriptSource[@]}"))
    WorkingList=("${WorkingList[@]/=/}")        # delete '=' signs from each item
    WorkingList=("${WorkingList[@]/:/ }")       # replace ':' separator with a space
    for Line in "${WorkingList[@]}"; do         # get each item into a variable
        IFS=' ' read -r -a Token <<< "${Line}"  # split item into parts
        LineNo=${Token[0]}
        VName="${Token[1]}"
        # ignore any which are internal shell variables
        if [[ " ${INTBASHVAR[*]} " =~ [[:space:]]${VName}[[:space:]] ]]; then
            continue
        fi
        # build dictionary of a variable assignments
        if [[ -z "${VarAssign[${VName}]=}" ]]; then
            VarAssign["${VName}"]="${LineNo} "  # initial entry
        else
            VarAssign["${VName}"]+="${LineNo} " # subsequent entry
        fi
    done
    # save list of undeclared variables
    VarUndeclare=($(comm -23 <(sort < <(printf "%s\n" "${!VarAssign[@]}")) \
                             <(sort < <(printf "%s\n" "${Variable[@]}") | uniq)))
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetVarUsage(){
# ------------------------------------------------------------------------------------ #
# Scan source code looking for any variable references and add them to a dictionary of #
# where used variable references.                                                      #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # NB undeclared constants and variables are declared in calling function
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    LineNum=""
    declare    VarRefs=""
    declare -a SortedList
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    for VName in ${Variable[@]}; do             # scan source for each declared variable
        VarRefs=""                              # reset string holding ref. line nos.
        # get results into working array (result format - 9999:XXXXXXXXXXXXX)
        readarray -t WorkingList \
                      < <(${GREP} -wno ${VName} < <(printf "%s\n" "${ScriptSource[@]}"))
        for Line in "${WorkingList[@]}"; do     # get each result into a variable
            LineNo=${Line%:*}
            # add to list of variable references
            VarWhereUsed["${VName}"]+="${LineNo} "
        done
    done
    # for each variable sort the references into order and remove duplicates
    for VName in ${!VarWhereUsed[@]}; do
        # get references into a working array
        SortedList=(${VarWhereUsed[${VName}]})
        VarWhereUsed["${VName}"]=""
        # sort into numeric ascending order and drop duplicates
        readarray -t SortedList < <(printf "%s\n" "${SortedList[@]}" | sort -nu)
        for LineNum in ${SortedList[@]}; do
            # format line number as 4 digits with leading zeros
            LineNum="0000${LineNum}"
            LineNum="${LineNum:(-4)} "
            VarWhereUsed["${VName}"]+="${LineNum}"
        done
    done
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetVariableDetails(){
# ------------------------------------------------------------------------------------ #
# Called to process a variable definition source line. Stores the details in several   #
# dictionaries; Variable, Attribute and Initial, all of which are keyed by the line    #
# number where they are declared.                                                      #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DEFAULT="untyped"
    declare -r CONSTANT="untyped"
    declare -r INTEGER="integer"
    declare -r INDIRECT="pointer"
    declare -r ARRAY="array"
    declare -r DICT="dictionary"
    declare -r INTCONST="integer"
    declare -r CONSTARRAY="array"
    declare -r CONSTDICT="dictionary"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # NB current source line in 'Line' also split into array 'Token'
    declare    TokenWork=""             # 'Token' item work area
    declare    Name=""                  # variable name
    declare    Init=""                  # variable initial value, if any
    declare    Attrib=""                # variable attributes work area
    declare -i Constant=${FALSE}        # flag if variable is a constant
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # NB 'Token' populated by calling function
    # check if any attributes defined
    if [[ "${Token[2]:0:1}" != '-' ]]; then
        # no, so insert default attribute string
        Token=("${Token[0]}" "${Token[1]}" '--' "${Token[@]:2}" )
    fi
    LineNo=${Token[0]}                  # get the line number where variable declared
    TokenWork="${Token[2]}"             # get variable attributes
    # convert declare option to human readable string
    case "${TokenWork}" in
        -r) Attrib="${CONSTANT}"
            Constant=${TRUE}
            ;;
        -i) Attrib="${INTEGER}"
            ;;
        -a) Attrib="${ARRAY}"
            ;;
        -A) Attrib="${DICT}"
            ;;
        -n) Attrib="${INDIRECT}"
            ;;
        -ir|-ri) Attrib="${INTCONST}"
                 Constant=${TRUE}
            ;;
        -ar|-ra) Attrib="${CONSTARRAY}"
                 Constant=${TRUE}
            ;;
        -Ar|-rA) Attrib="${CONSTDICT}"
                 Constant=${TRUE}
            ;;
        --) Attrib="${DEFAULT}"
            ;;
    esac
    TokenWork="${Token[@]:3}"           # get 'name=value'
    Name="${TokenWork/=*/}"             # get variable name
    Name=${Name//$'\n'/}                # remove any extraneous newlines
    Name="${Name%"${Name##*[![:blank:]]}"}" # remove trailing whitespace characters
    # update maximum variable name length if required
    MaxVNLen=$(( ${#Name} > MaxVNLen ? ${#Name} : MaxVNLen ))
    if [[ "${TokenWork}" == *=* ]]; then # if equals '=' present then
        Init="${TokenWork/*=/}"         # get initial value
        # check if initial value is an argument
        if [[ "${Init}" =~ \$\{([1-9])[:]?[}]? ]]; then
            # add description and argument number
            Attrib="arg #${BASH_REMATCH[1]} ${Attrib}"
        else
            # if not a constant (which are always initialised)
            # indicate if an initial value
            if (( Constant == FALSE )); then
                Attrib+=" initialised"
            fi
        fi
    fi
    Attribute[${LineNo}]="${Attrib}"    # save in dictionary
    Variable[${LineNo}]="${Name}"       # save name to dictionary
    VarWhereUsed[${Name}]=""            # initialise usage dictionary
    Initial[${LineNo}]="${Init-}"       # save initial value allowing for null values
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnPrintReport(){
# ------------------------------------------------------------------------------------ #
# Analyse the collected function and variable data and produce a consolidated where    #
# used report.                                                                         #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  CALLEDBY="Called by:"
    declare -r  CALLS="    Calls:"
    declare -r  FUNLSTHDR="Functions in Order of Definition"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Reference                # current line number + function reference item
    declare -a Calls                    # list of functions called by a function
    declare -i Start                    # basename function start line number
    declare -i End                      # basename function end line number
    declare -i Printed                  # whether any items printed or not
    declare -a Functions                # sorted list of function names
    declare    BaseFunction=""          # current function being reported on
    declare -i FunCount=0               # count of function details printed
    declare -- FunHdr=""
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # print list of functions in order of start line no. (i.e. order defined)
    for Start in $(printf "%s\n" "${Function[@]%:*}" | sort -n); do
        FunCount+=1
        BaseFunction="$(fnGetFunctionByLineNo ${Start})"
        End=${Function[${BaseFunction}]#*:}
        Functions+=("$(printf "%02d) %*s (%04d-%04d)" ${FunCount} ${MaxFNLen} \
                                        "${BaseFunction}" ${Start} ${End})")
    done
    fnPrintBlock ${FALSE} "${FUNLSTHDR}" "-" "" "Functions" 0 ${TRUE}
    # get sorted list of function names
    readarray -t Functions < <(printf '%s\n' "${!Function[@]}" | sort -n)
    # build the report with one section for each function in turn
    FunCount=0
    for BaseFunction in ${Functions[@]}; do
        FunCount=$((FunCount+1))
        fnProgress ${FunCount} ${NoFunctions} "Printing Report"
        # get start and end line numbers
        Start=${Function[${BaseFunction}]%:*}
        End=${Function[${BaseFunction}]#*:}
        # print function block header
        FunHdr="$(printf "%s (%04d-%04d)" "${BaseFunction}" ${Start} ${End})"
        # print 'calls' section
        # ---------------------
        Calls=(${FunCalls[${BaseFunction}]})
        fnPrintBlock "" "${FunHdr}" "-" "${CALLS}" "Calls" ${MaxFNLen}
        # print 'called by' section
        # -------------------------
        Calls=(${FunCalled[${BaseFunction}]})
        fnPrintBlock "" "" "-" "${CALLEDBY}" "Calls" ${MaxFNLen} ${TRUE}
        # print variables
        fnVariableBlock ${TRUE}         # print constants defined in function
        fnVariableBlock ${FALSE}        # print variables defined in function
    done
    # create primary code section
    BaseFunction="${MAINCODE}"
    # get start and end line numbers
    Start=${PrimaryStart}
    End=${NumLines}
    # print header
    FunHdr="$(printf "%s (%04d-%04d)" "${BaseFunction}" ${Start} ${End})"
    # print 'calls' section
    Calls=(${FunCalls[${BaseFunction}]})
    fnPrintBlock "" "${FunHdr}" "-" "${CALLS}" "Calls" ${MaxFNLen} ${TRUE}
    # print variables
    fnVariableBlock ${TRUE}         # print constants defined in function
    fnVariableBlock ${FALSE}        # print variables defined in function
    fnUndeclared                    # print undeclared variables
    fnWhereUsed                     # print variable cross reference
    fnUnreferenced                  # print declared but unreferenced variables
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnPrintBlock(){
# ------------------------------------------------------------------------------------ #
# Print a block of data either by rows or in columns. The data to be printed is passed #
# across as an array. The function takes up to 7 arguments.                            #
#                                                                                      #
# Arguments: 1 - boolean, TRUE=print by rows, FALSE=print in columns                   #
#                defaults to TRUE                                                      #
#            2 - heading text to precede block. Can be blank when no header is printed #
#            3 - underline character, if longer than 1 character only 1st taken        #
#            4 - subheading to be printed at start of 1st line                         #
#            5 - name of array (list) containing data to be printed. If no data the    #
#                keyword 'Null' can be used.                                           #
#            6 - width each data field is to be printed in. If not supplied the        #
#                length of the longest data item is taken                              #
#            7 - TRUE=print blank line after block. Defaults to FALSE                  #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ir ROWS=${1:-${TRUE}}
    declare -r  HEADING="${2}"
    declare -r  UNDERLINE="${3}"
    declare -r  SUBHEADING="${4}"
    declare -ir FLDWIDTH=${6:-0}
    declare -ir BLANKLINE=${7:-${FALSE}}
    declare -r  NODATA="** None **"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a Null=("${NODATA}")
    declare -n _PrtFld="${5}"
    declare -i FieldCount=0
    declare -- Field
    declare -i MaxFieldWidth=0
    declare -i PrintWidth=0
    declare -i PrintColumns=0
    declare -i Column=0
    declare -i ColumnLength=0
    declare -- SubHead=""
    declare -i FieldNo=0
    declare -i FieldCol=0
    declare -i Skip=${FALSE}
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # if requested print the heading
    if [[ "${HEADING}" != "" ]]; then
        fnMsg "${STDOUT}" "%s\n" "${HEADING}"
        # is an underline requested?
        if [[ "${UNDERLINE}" != "" ]]; then
            fnMsg "${STDOUT}" "%s\n" "$(fnCopies "${UNDERLINE:0:1}" ${#HEADING})"
        fi
    fi
    # skip if null dataset supplied
    if (( ${#_PrtFld[@]} == 1 )); then
        if [[ "${_PrtFld[0]}" == "${NODATA}" ]]; then
            Skip=${TRUE}
        fi
    fi
    if (( Skip == FALSE )); then
        # get dataset parameters
        FieldCount=${#_PrtFld[@]}        # count number of data fields
        if (( FLDWIDTH == 0 )); then     # if field width not given
            for Field in "${_PrtFld[@]}"; do # get length of longest field
                MaxFieldWidth=$(( ${#Field} > MaxFieldWidth ? ${#Field} : MaxFieldWidth ))
            done
        else
            MaxFieldWidth=${FLDWIDTH}
        fi
        MaxFieldWidth+=1                 # single space between columns
        # calculate print width
        PrintWidth=${COLS}               # maximum available columns
        if [[ SUBHEADING != "" ]]; then  # if there's a subheading
            PrintWidth=$(( PrintWidth-${#SUBHEADING} )) # adjust the max. print width
        fi
        SubHead="${SUBHEADING}"
        PrintColumns=$(( PrintWidth / MaxFieldWidth )) # how many columns to print
        # now print the data
        if (( ROWS == TRUE )); then
            # print fields by row (i.e. horizontally)
            Column=0
            for Field in "${_PrtFld[@]}"; do # print data
                Column+=1
                if (( Column == 1 )); then
                    # print subheading even if blank
                    fnMsg "${STDOUT}" "%s" "${SubHead}"
                    # replace with blanks after first
                    SubHead="$(fnCopies " " ${#SUBHEADING})"
                fi
                fnMsg "${STDOUT}" "%*s" ${MaxFieldWidth} "${Field:0:${MaxFieldWidth}}"
                if (( Column == PrintColumns )); then
                    fnMsg "${STDOUT}" "\n"
                    Column=0
                fi
            done
            if (( Column > 0 )); then
                fnMsg "${STDOUT}" "\n"
            fi
        else
            # print fields by column (i.e. vertically)
            ColumnLength=$(( FieldCount / PrintColumns ))
            for (( FieldNo=0; FieldNo<=ColumnLength; FieldNo++ )); do
                for (( Column=0; Column<PrintColumns; Column++ )); do
                    if (( Column == 0 )); then
                        # print subheading even if blank
                        fnMsg "${STDOUT}" "%s" "${SubHead}"
                        # replace with blanks after first
                        SubHead="$(fnCopies " " "${#SUBHEADING}")"
                    fi
                    FieldCol=$(( FieldNo+Column*(ColumnLength+1) ))
                    if (( FieldCol < FieldCount )); then
                        fnMsg "${STDOUT}" "%*s" ${MaxFieldWidth} \
                                           "${_PrtFld[${FieldCol}]:0:${MaxFieldWidth}}"
                    fi
                done
                fnMsg "${STDOUT}" "\n"
            done
        fi
        if (( FieldCount == 0 )); then
            fnMsg "${STDOUT}" "%s%*s\n" "${SubHead}" ${MaxFieldWidth} "${NODATA}"
        fi
    fi
    if (( BLANKLINE == TRUE )); then
        fnMsg "${STDOUT}" "\n"
    fi
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnVariableBlock(){
# ------------------------------------------------------------------------------------ #
# Prints a variable detail block for the current function. Takes a single argument     #
# that specifies whether only constants or variables are included. The block is sorted #
# by variable name.                                                                    #
#                                                                                      #
# Arguments: print constants flag TRUE|FALSE                                           #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  CONSTHDR="Constants:"
    declare -r  VARHDR="Variables:"
    declare -ir CONSTANTS=${1}
    declare -ir FW=30
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i LineNo
    declare    VarName
    declare -a VarDetail
    declare -a VarSort
    declare    Heading=""
    declare -i FieldWidth=0
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    VarDetail=("${VarDetail[@]:1:0}") # clear down array
    VarSort=("${VarSort[@]:1:0}")     # clear down array
    # iterate over the variable definition line numbers
    for LineNo in ${!Variable[@]}; do
        # is this definition for current function
        if [[ "$(fnGetFunctionByLineNo ${LineNo})" == "${BaseFunction}" ]]; then
            VarName="${Variable[${LineNo}]}"
            # if this a constant (i.e. all uppercase)
            # and constants are not required
            if [[ ${VarName} == ${VarName^^} ]]; then
                # declaration is for a constant
                if (( CONSTANTS == FALSE )); then
                    # and constants are not wanted
                    continue
                fi
            else
                # declaration is for a variable
                if (( CONSTANTS == TRUE )); then
                    # and constants are wanted
                    continue
                fi
            fi
            VarDetail+=("$(printf "%*s(%04d) %-*.*s" ${MaxVNLen}              \
                                                    "${VarName}"              \
                                                     ${LineNo}                \
                                                     ${FW} ${FW}              \
                                                    "${Attribute[${LineNo}]}" \
                                                                            )")
        fi
    done
    if (( CONSTANTS == TRUE )); then
        Heading="${CONSTHDR}"
    else
        Heading="${VARHDR}"
    fi
    # if no entries set a default field width
    # otherwise get sorted list of variable details
    if (( ${#VarDetail[@]} == 0 )); then
        FieldWidth=$(( MaxVNLen+6 ))
    else
        readarray -t VarSort < <(printf '%s\n' "${VarDetail[@]}" | sort)
    fi
    fnPrintBlock "" "" "" "${Heading}" "VarSort" ${FieldWidth} ${TRUE}
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnUndeclared(){
# ------------------------------------------------------------------------------------ #
# Prints an undeclared variable detail block.                                          #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  UNDECLARED="Undeclared Variables"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a VarSort
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    VarSort=("${VarSort[@]:1:0}")     # clear down array
    # only get sorted list of variable details if data present
    if (( ${#VarUndeclare[@]} > 0 )); then
        readarray -t VarSort < <(printf '%s\n' "${VarUndeclare[@]}" | sort)
    fi
     # get sorted list of variable details
    fnPrintBlock "" "${UNDECLARED}" "-" "" "VarSort" ${MaxFVLen} ${TRUE}
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnWhereUsed(){
# ------------------------------------------------------------------------------------ #
# Print the variable where used block, wrapping if data longer than a line.            #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  HEADING="Variable Cross Reference"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a VarSort                  # used to hold sorted list of variable names
    declare -a Data
    declare    Header=""                # heading text for print
    declare -i Length=0                 # length of data to be printed
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnPrintBlock "" "${HEADING}" "-" "" "Null" 0
    # get the variable names and sort them
    readarray -t VarSort < <(printf "%s\n" "${!VarWhereUsed[@]}" | sort)
    for VName in ${VarSort[@]}; do
        Data=( ${VarWhereUsed[${VName}]} )
        printf -v Header "%*s:" ${MaxVNLen} "${VName}"
        fnPrintBlock "" "" "" "${Header}" "Data" 0
    done
    fnPrintBlock "" "" "" "" "Null" 0 ${TRUE}
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnUnreferenced(){
# ------------------------------------------------------------------------------------ #
# Print the unreferenced variable block.                                               #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r UNREFHDR="Declared But Unreferenced Variables:"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a VarSort
    declare -a UnReferenced
    declare -i Length=0
    declare -i Printed=${FALSE}
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    VarSort=("${VarSort[@]:1:0}")     # clear down array
    UnReferenced=("${UnReferenced[@]:1:0}")     # clear down array
    # get sorted list of variable details
    readarray -t VarSort < <(printf "%s\n" ${!VarWhereUsed[@]} | sort)
    # build a list of unreferenced variables - single entry = initial declaration
    for VName in ${VarSort[@]}; do
        Length=${#VarWhereUsed[${VName}]}
        if (( Length <= 5 )); then
            UnReferenced+=("${VName}:${VarWhereUsed[${VName}]}")
        fi
    done
    fnPrintBlock "" "${UNREFHDR}" "-" "" "UnReferenced" $(( MaxFVLen+6 ))
    return ${Result}
}
function fnGetFunctionByLineNo(){
# ------------------------------------------------------------------------------------ #
# Given a line number, if it's within a function definition, return the name of the    #
# function where the line occurs, otherwise indicate it's in primary code.             #
#                                                                                      #
# Arguments: integer, line number                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r TARGET=${1}              # find function corresponding to this line no.
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Start=0                  # current function start line no.
    declare -i End=0                    # current function end line no.
    declare    FName                    # current function name
    declare -i Found=${FALSE}           # flag if target is in current function
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    for FName in ${!Function[@]}; do    # scan through function names
        Start=${Function[${FName}]%:*} # get the start line no.
        End=${Function[${FName}]#*:}   # get the end line no.
        # is target line number within range of this function
        if [[ ${TARGET} -ge ${Start} ]]; then
            if [[ ${TARGET} -le ${End} ]]; then
                Found=${TRUE}               # flag match
                break                       # exit from function name search
            fi
        fi
    done
    if (( Found == FALSE )); then       # no match found so
        FName="${MAINCODE}"             # say target is in main code
    fi
    fnEcho ${FName}                     # return the function name
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnProgress(){
# ------------------------------------------------------------------------------------ #
# Displays a progress bar on stderr using the supplied arguments. Arguments 3-7 are    #
# optional.                                                                            #
#                                                                                      #
# Arguments: 1) integer, the current value for the progress bar                        #
#            2) integer, the maximum value for the progress bar                        #
#            3) string,  initial text for progress bar (default 'Progress')            #
#            4) integer, length of bar in characters (default 40)                      #
#            5) string,  character to be used for completed progress                   #
#            6) string,  character to be used for uncompleted progress                 #
#            7) integer, scaling factor for computing percentage (no. decimal places)  #
#                                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r CURRENT="${1}"
    declare -r TOTAL="${2}"
    declare -r TITLE="${3:-Progress}"
    declare -r BARSIZE="${4:-40}"
    declare -r BARDONE="${5:-#}"
    declare -r BARTODO="${6:--}"
    declare -r BARSCALE="${7:-2}"
    declare -r BARFMT="${CLEOL}%s : [%s%s] %s%% (%d/%d)\r"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Percent
    declare    Done
    declare    ToDo
    declare    DoneSubBar=""
    declare    ToDoSubBar=""
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # calculate the progress in percentage
    Percent=$(bc <<< "scale=${BARSCALE}; 100 * ${CURRENT} / ${TOTAL}" )
    # The number of done and todo characters
    Done=$(bc <<< "scale=0; ${BARSIZE} * ${Percent} / 100" )
    ToDo=$(bc <<< "scale=0; ${BARSIZE} - ${Done}" )
    # build the done and todo sub-bars
    DoneSubBar=$(fnCopies "${BARDONE}" ${Done})
    ToDoSubBar=$(fnCopies "${BARTODO}" ${ToDo})
    # output the bar
    fnMsg "${STDERR}" "${BARFMT}" "${TITLE}" "${DoneSubBar}" \
                      "${ToDoSubBar}" "${Percent}" ${CURRENT} ${TOTAL}
    if (( TOTAL == CURRENT )); then
        fnMsg "${STDERR}" "%s" "\n"     # final call
    fi
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnUnexpectedError(){
# ------------------------------------------------------------------------------------ #
# called when an unexpected error occurs to display context information and then       #
# terminate the run.                                                                   #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ir RC=${?}
    declare -r  ERRMSGa="\nUNEXPECTED ERROR in function %s()"
    declare -r  ERRMSG="${ERRMSGa} in command near line %s:\n"
    declare -ir ERRLINE=${BASH_LINENO[1]}
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare     Sourceline="$(head -n${ERRLINE} ${0} | tail -1)"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # don't report errors in subshells only top level (0)
    if (( BASH_SUBSHELL > 0 )); then
        return ${RC}
    fi
    fnMsg "${BOTH}" "${ERRMSG}" "${FUNCNAME[1]}" "${BASH_LINENO[0]}"
    fnMsg "${BOTH}" "" "${Sourceline}"
    fnMsg "${BOTH}" "" "returned code: ${RC}"
    fnMsg "${BOTH}" "" "Run terminated."
    exit ${RC}
}
# ------------------------------------------------------------------------------------ #
function fnEcho(){
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
#                                                                                      #
# Arguments: 1...n) data to be displayed                                               #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnMsg(){
# ------------------------------------------------------------------------------------ #
# Output the argument string to either stdout or stderr or both depending on the value #
# of the 1st argument. The 2nd argument must be a format string as used by the         #
# 'printf' command. The remaining arguments are used as arguments to the 'printf'      #
# command.                                                                             #
#                                                                                      #
# Arguments: 1)   where output is to be directed, see GLOBAL CONSTANTS section.        #
#            2)   format string valid for use by 'printf'                              #
#            3-n) strings to be output                                                 #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r TARGET="${1:-${STDOUT}}" # where output is to go
    declare -r FORMAT="${2:-%b }"       # printf format string
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Stdout=""
    declare    Stderr=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    shift 2                             # drop arguments 1 & 2
    # output to specified target(s)
    case "${TARGET}" in
        "${STDOUT}") # output to stdout
            printf -v Stdout "${FORMAT}" "${@}"
            fnEcho "${Stdout}"
            ;;
        "${STDERR}") # output to stderr
            printf -v Stderr "(%s) ${FORMAT}" "$(date "+%T")" "${@}"
            fnEcho "${Stderr}" >&2
            ;;
        "${BOTH}") # output to both stdout & stderr
            printf -v Stdout "${FORMAT}" "${@}"
            printf -v Stderr "(%s) ${FORMAT}" "$(date "+%T")" "${@}"
            fnEcho "${Stdout}"
            fnEcho "${Stderr}" >&2
            ;;
    esac
    return ${TRUE}
}
# ------------------------------------------------------------------------------------ #
function fnCopies(){
# ------------------------------------------------------------------------------------ #
# Repeats a string value a specified number of times.                                  #
#                                                                                      #
# Arguments:                                                                           #
#     1) string_expression - a string of one or more characters.                       #
#     2) integer - number of times string is to be repeated.                           #
# Returns:                                                                             #
#     a string.                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -r  SOURCE="${1}"
    declare -ir COPIES=${2}
    # set up a literal of spaces a long as the number of times source is to be repeated
    declare -r  SPACES="$(printf "%*s" ${COPIES} " ")"
    # replace each space character with the source string
    declare -r  RESULT="${SPACES// /${SOURCE}}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if (( COPIES == 0 )); then
        fnEcho ""
    else
        fnEcho "${RESULT}"
    fi
    return
}
# ------------------------------------------------------------------------------------ #
function fnWhereis(){
# ------------------------------------------------------------------------------------ #
# Get full path to executable using output from 'whereis'.                             #
#                                                                                      #
# Arguments: name of executable                                                        #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get 2nd word of whereis output
    fnEcho "$(cut -d " " -f 2 <<<"$(whereis -b ${1})")"
    return
}
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  SCRIPTSOURCE="${1}"        # full source file path and name
declare -r  SOURCENAME="${1##*/}"      # base name of source file
# get full paths to executables - 2nd word of 'whereis' output
declare -r  GREP="$(fnWhereis "grep")"
# used for calls to fnMsg to indicate output target
declare -r  STDOUT="C"                 # stdout
declare -r  STDERR="E"                 # stderr
declare -r  BOTH="2"                   # stdout & stderr
declare -ir COLS=$(tput cols)          # get display width in characters
declare -r  CLEOL="$(tput el)"         # clear to end of line
# ------------------------ Main Dictionary Structures ---------------------------- #
#   Function[function name]--+--start-line
#                            |
#                            +--end-line
#
#   FunCalls[function name]--(function function function....function)
#
#   FunCalled[function name]--(function function function....function)
#
#   Variable[declared line]--variable-name
#
#   Attribute[declared line]--[attribute description]
#
#   Initial[declared line]--'literal value'
#
#   VarWhereUsed[variable-name]--(ref:ref:ref:....ref)
#
# where: 'ref' is "line number-function name"
# -------------------------------------------------------------------------------- #
# dictionaries & variables related to processing function references
declare -A Function                 # dictionary of function names, key=function
                                    # name; values=start line no.:end line no.
declare -A FunCalls                 # dictionary of functions called by a function,
                                    # called functions are concatenated together
                                    # separated by spaces,indexed by function name.
declare -A FunCalled
declare -i NoFunctions=0            # count of function definitions found in source
declare -i PrimaryStart=0           # line number where primary code starts
# dictionaries which hold variable attributes, key="declared line number"
declare -A Variable                 # variable name
declare -A Attribute                # attributes of variables
declare -A Initial                  # if any, initial value
declare -a VarUndeclare             # list of variables which have not been declared
declare -A VarWhereUsed             # dictionary of in-code variable references,
                                    # line numbers are concatenated together
                                    # separated by space
# variables related to processing variable references
declare -i NoVariables=0            # count of number of variable declarations
declare -i NoUniqVars=0             # number of unique variable declarations
# list of internal bash variable names for elimination from user lists
declare -ar INTBASHVAR=(            "#"                     "*"
            "@"                     "0"                     "1"
            "2"                     "3"                     "4"
            "5"                     "6"                     "7"
            "8"                     "9"                     "?"
            "!"                     "$"                     "-"
            "BASH"                  "BASHOPTS"              "BASHPID"
            "BASH_ALIASES"          "BASH_ARGC"             "BASH_ARGV"
            "BASH_ARGV0"            "BASH_CMDS"             "BASH_COMMAND"
            "BASH_COMPAT"           "BASH_ENV"              "BASH_EXECUTION_STRING"
            "BASH_LINENO"           "BASH_LOADABLES_PATH"   "BASH_REMATCH"
            "BASH_SOURCE"           "BASH_SUBSHELL"         "BASH_VERSINFO"
            "BASH_VERSION"          "BASH_XTRACEFD"         "CDPATH"
            "CHILD_MAX"             "COLUMNS"               "COMPREPLY"
            "COMP_CWORD"            "COMP_KEY"              "COMP_LINE"
            "COMP_POINT"            "COMP_TYPE"             "COMP_WORDBREAKS"
            "COMP_WORDS"            "COPROC"                "DIRSTACK"
            "EMACS"                 "ENV"                   "EPOCHREALTIME"
            "EPOCHSECONDS"          "EUID"                  "EXECIGNORE"
            "FCEDIT"                "FIGNORE"               "FUNCNAME"
            "FUNCNEST"              "GLOBIGNORE"            "GROUPS"
            "HISTCMD"               "HISTCONTROL"           "HISTFILE"
            "HISTFILESIZE"          "HISTIGNORE"            "HISTSIZE"
            "HISTTIMEFORMAT"        "HOME"                  "HOSTFILE"
            "HOSTNAME"              "HOSTTYPE"              "IFS"
            "IGNOREEOF"             "INPUTRC"               "INSIDE_EMACS"
            "LANG"                  "LC_ALL"                "LC_COLLATE"
            "LC_CTYPE"              "LC_MESSAGES"           "LC_NUMERIC"
            "LC_TIME"               "LINENO"                "LINES"
            "MACHTYPE"              "MAIL"                  "MAILCHECK"
            "MAILPATH"              "MAPFILE"               "OLDPWD"
            "OPTARG"                "OPTERR"                "OPTIND"
            "OSTYPE"                "PATH"                  "PIPESTATUS"
            "POSIXLY_CORRECT"       "PPID"                  "PROMPT_COMMAND"
            "PROMPT_DIRTRIM"        "PS0"                   "PS1"
            "PS2"                   "PS3"                   "PS4"
            "PWD"                   "RANDOM"                "READLINE_ARGUMENT"
            "READLINE_LINE"         "READLINE_MARK"         "READLINE_POINT"
            "REPLY"                 "SECONDS"               "SHELL"
            "SHELLOPTS"             "SHLVL"                 "SRANDOM"
            "TIMEFORMAT"            "TMOUT"                 "TMPDIR"
            "UID")
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i Result=${TRUE}
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#   -e exit immediately if a command exits with a non-zero status.
#   -E the ERR trap is inherited by shell functions.
#   -u treat unset variables as an error when substituting.
set -Eeu                                # set shell options for run
trap 'fnUnexpectedError' ERR            # trap any errors
fnMain "${@}"                           # invoke main function
Result=${?}
exit ${Result}
