#!/bin/bash
# ======================================================================================
#          FILE: switchapp.sh
# ======================================================================================
#   DESCRIPTION: Move the focus either forwards through active windows or backwards
#                through active windows. Takes one argument, a string either 'Prev' or
#                'Next' (case insensitive). No default.
# ======================================================================================
#         USAGE: switchapp [Prev|Next]
#         NEEDS: wmctrl, xdotool
#     COPYRIGHT: Keith Watson (swillber@gmail.com)
#       VERSION: 1.0.3
#  DATE CREATED: 27-Dec-2021 22:53
#  LAST CHANGED: 30-Dec-2021 23:04
#    KNOWN BUGS: ---
# ======================================================================================
#  LICENSE:
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or (at my option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with this
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
#  Fifth Floor, Boston, MA 02110-1301, USA.
# ======================================================================================
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
#
# get info about size of current display
declare -r  DIMENSIONS="$(cut -d' ' -f7 < <(xdpyinfo | grep 'dimensions:'))"
declare -ir WIDTH=${DIMENSIONS%x*}      # extract width of display
declare -ir HEIGHT=${DIMENSIONS#*x}     # extract height of display
declare -ir EXPIRE=1500                 # how long to display notification box(msecs)
declare -r  FOCUS="#"
declare -r  UNFOCUS="-"
#                                                                                      #
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare    Direction
declare -a Window                       # working array to hold subset of active windows
declare -a Pid                          # process ids of active windows
declare -i WindowID=0                   # holder for desktop no. a window is on
declare -i WindowPid=0                  # holder for desktop no. a window is on
declare    WindowType=""
declare    WindowState=""
declare    SkipPager=""
declare    SkipTaskbar=""
declare    Application=""
declare    ListPrefix=""
# notify-send argument values
declare    Summary=""
declare    BodyText=""
declare    X=$(( WIDTH/2 - 200 ))
declare    Y=$(( HEIGHT/2 - 150 ))
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
Direction="${1}"                        # get the first argument
Direction="${Direction,,}"              # convert to lower case
case "${Direction}" in                  # validate argument
    prev)
        true
        ;;
    next)
        true
        ;;
    *)
        notify-send --icon=error --expire-time=${EXPIRE} \
                    "Task Switch Error"                  \
                    "Argument must be 'Prev' or 'Next'(case insensitive)."
        exit 1
        ;;
esac

IFS=$'\n'                              # set field separator to newline only
#if ! true ; then
# iterate over list of active windows picking only those that are active and selectable
while read -r Win; do
    # get main parameters for this window
    WindowID=${Win:0:10}
    # initialise state flags
    WindowType=""
    WindowState=""
    SkipPager="False"
    SkipTaskbar="False"
    WindowPid=0
    # examine details of this window
    while read -r Info; do
        case "${Info:10}" in
            "Normal")       WindowType="Normal";;
            "Dock")         WindowType="Dock";;
            "Focused")      WindowState="Focused";;
            "Skip Pager")   SkipPager="True";;
            "Skip Taskbar") SkipTaskbar="True";;
        esac
        # get the pid
        if [[ "${Info:6:11}" == "Process id:" ]]; then
            WindowPid="$(cut -d' ' -f9 < <(printf "%s" "${Info}"))"
        fi
    done < <(xwininfo -id ${WindowID} -wm)
    # if not a 'normal' window skip this entry
    if [[ "${SkipPager}" == "True" || \
          "${SkipTaskbar}" == "True" || \
          "${WindowType}" != "Normal" ]]; then
        continue
    fi
    # set a flag to indicate current focused window
    Ind="${UNFOCUS}"
    if [[ "${WindowState}" == "Focused" ]]; then
        Ind="${FOCUS}"
    fi
    # this is an 'active' window, append it to an array
    Window+=("${Ind}${Win}")
    # store the pid
    Pid+=("${WindowPid}")
done < <(wmctrl -l | sort -k2 -n)
# get number of windows in list
WinCount=${#Window[@]}
# no items found, output error message and exit
if (( WinCount == 0 ));then
    notify-send.sh --icon=error --expire-time=${EXPIRE} "Task Switch Error" \
                                                        "No active windows found."
    exit 1
fi
# indicate if any window has focus
Focus=-1
for (( W=0; W<WinCount; W++ )); do
    if [[ "${Window[$W]:0:1}" == "${FOCUS}" ]];then
        Focus=${W}
    fi
done
# if set reset focused window flag
if (( Focus >= 0 )); then
    Window[${Focus}]="${UNFOCUS}${Window[${Focus}]:1}"
fi
# move the focus either forwards or back depending on the supplied argument
# NB use modulus to ensure movement is circular
if [[ "${Direction}" == "next" ]]; then
    Focus=$(( (Focus+1)%WinCount ))
else
    Focus=$(( (Focus+WinCount-1)%WinCount ))
fi
wmctrl -i -a ${Window[$Focus]:1:10}     # activate the window now indicated by the focus
Window[${Focus}]="${FOCUS}${Window[${Focus}]:1}" # set focused window flag
# display a notification with list of tasks
Summary="Task Switch"
BodyText=""
for (( W=0; W<WinCount; W++ )); do
    # prefix each task with its desktop number
    ListPrefix=${Window[$W]:12:3}
    ListPrefix=$(( ListPrefix+1 ))
    ListPrefix+=") "
    # get the base name of this task's application
    Application="$(ps -o args --no-header ${Pid[$W]})"
    Application=${Application##*/}
    # highlight the task which has the focus
    if [[ "${Window[$W]:0:1}" == "${FOCUS}" ]];then
        BodyText+="<i><b>${ListPrefix}${Application}</b></i>\n"
    else
        BodyText+="${ListPrefix}${Application}\n"
    fi
done
# display the task list at the centre of the display
notify-send.sh  --expire-time=${EXPIRE} \
                --hint="int:x:${X}" --hint="int:y:${Y}" \
                "${Summary}" "${BodyText}"
exit
