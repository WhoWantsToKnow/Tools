#!/bin/bash
# ==================================================================================== #
#         FILE: movegkrellm.sh                                                         #
# ==================================================================================== #
#  DESCRIPTION: This script is intended to be run at session start. It waits for       #
#               gkrellm to start and then moves the window to the highest numbered     #
#               desktop to a position specified by 2 command line arguments;           #
#               1) X position of upper left corner of window                           #
#               2) Y position of upper left corner of window                           #
#                                                                                      #
# ==================================================================================== #
#        USAGE: movegkrellm                                                            #
#        NEEDS: xdotool                                                                #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 2.3.1                                                                  #
# DATE CREATED: not recorded                                                           #
# LAST CHANGED: 07-Nov-2022 22:20                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2018 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
#
# get base name of script from argument 0 (see bash parameter expansion)
declare -r  SCRIPTNAME="${0##*/}"
# get the script version number from the source header
declare -r  VERSION="$(cut -c17-25 <<< "$(grep -m1 "VERSION:" "${0}")")"
#
declare -ir COLUMN=${1}         # X position of upper left corner of window
declare -ir ROW=${2}            # Y position of upper left corner of window
declare -ir DESKTOPS=$(xdotool get_num_desktops)
declare -ir HIGHEST=$(( DESKTOPS - 1 ))
declare -r  TITLE="gkrellm"
# block until target window is visible and save window id
declare -r  WINDOW=$(xdotool search --sync --onlyvisible --class "${TITLE}")
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#                                                                                      #
# move target to highest numbered desktop (0=1st)
xdotool set_desktop_for_window ${WINDOW} ${HIGHEST}
# move target to required position
xdotool windowmove ${WINDOW} ${COLUMN} ${ROW}
