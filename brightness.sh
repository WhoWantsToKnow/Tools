#!/usr/bin/env bash
# =====================================================================================#
#          FILE: brightness.sh                                                         #
# =====================================================================================#
#   DESCRIPTION: Wrapper script for xbacklight that allows increasing/decreasing the   #
#                display brightness and provides visual feedback via an on-screen      #
#                notification.                                                         #
# =====================================================================================#
#         USAGE: brightness Up|Down  (arguments not case sensitive)                    #
#         NEEDS: xbacklight, notify-send.sh, Papirus icon set                          #
#     COPYRIGHT: Keith Watson (swillber@gmail.com)                                     #
#       VERSION: 1.1.1                                                                 #
#  DATE CREATED: ???                                                                   #
#  LAST CHANGED: 30-Sep-2020 22:30
#    KNOWN BUGS: ---                                                                   #
#          TODO: ---                                                                   #
# =====================================================================================#
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at my option) any later version.   #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110-1301, USA.                                            #
# =====================================================================================#
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnUp() {
# ------------------------------------------------------------------------------------ #
# Increases the display brightness by the globally defined adjustment using xbacklight #
# Arguments: none                                                                      #
# Returns: nothing                                                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    xbacklight -inc ${ADJUSTMENT}
}
# ------------------------------------------------------------------------------------ #
function fnDown() {
# ------------------------------------------------------------------------------------ #
# Decreases the display brightness by the globally defined adjustment using xbacklight #
# Arguments: none                                                                      #
# Returns: nothing                                                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    xbacklight -dec ${ADJUSTMENT}
}
# ------------------------------------------------------------------------------------ #
function fnNotify() {
# ------------------------------------------------------------------------------------ #
# Using notify-send.sh (a replacement for notify-send that extends its functionality)  #
# output an on screen notification that feeds back the changed display brightness.     #
# Arguments: none                                                                      #
# Returns: nothing                                                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  APPNAME="Display Brightness"
    declare -r  ICONPATH="/usr/share/icons/Flat-Remix-Blue/apps/scalable/"
    declare -r  ICONFULL="${ICONPATH}notification-display-brightness-full.svg"
    declare -r  ICONHIGH="${ICONPATH}notification-display-brightness-high.svg"
    declare -r  ICONMEDIUM="${ICONPATH}notification-display-brightness-medium.svg"
    declare -r  ICONLOW="${ICONPATH}notification-display-brightness-low.svg"
    declare -r  ICONOFF="${ICONPATH}notification-display-brightness-off.svg"
    declare -r  NOTIFYIDFILE="/tmp/videobrightnessnotifyid"
    declare -r  FULLBLOCK=$(printf '\xE2\x96\x88\n')
    declare -ir TIMEOUT=1000                # milliseconds
    declare -ir FULL=99
    declare -ir HIGH=66
    declare -ir MEDIUM=33
    declare -ir LOW=1
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i BRIGHTNESS=0
    declare -i MAXBRIGHTNESS=0
    declare -i SCORE=50
    declare    NOTIFYICON=""
    declare    TITLE=""
    declare    TEXT=""
    declare    WORK=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    BRIGHTNESS=$(</sys/class/backlight/amdgpu_bl0/brightness)
    MAXBRIGHTNESS=$(</sys/class/backlight/amdgpu_bl0/max_brightness)
    if (( BRIGHTNESS == MAXBRIGHTNESS )); then
        SCORE="100"
    else
        SCORE="$(( BRIGHTNESS * 100 / MAXBRIGHTNESS ))"
    fi
    if (( SCORE > FULL )); then
        NOTIFYICON="${ICONFULL}"
    elif (( SCORE > HIGH )); then
        NOTIFYICON="${ICONHIGH}"
    elif (( SCORE > MEDIUM )); then
        NOTIFYICON="${ICONMEDIUM}"
    elif (( SCORE > LOW )); then
        NOTIFYICON="${ICONLOW}"
    else
        NOTIFYICON="${ICONOFF}"
    fi
    TITLE="${APPNAME}$(printf " %d%%" ${SCORE})"
    if (( SCORE == 0 )); then
        TEXT=" "
    else
        SCORE=$(( SCORE / 5 ))
        WORK=$(printf "%-${SCORE}s" "${FULLBLOCK}")
        TEXT="${WORK// /${FULLBLOCK}}"
    fi
    notify-send.sh \
            --urgency=critical \
            --app-name="${TITLE}" \
            --hint=int:value:${SCORE} \
            --icon="${NOTIFYICON}" \
            --replace-file="${NOTIFYIDFILE}" \
            --expire-time=${TIMEOUT} \
            "${TEXT}"
}
# ------------------------------------------------------------------------------------ #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -r  DIRECTION=${1,,}            # force argument to lowercase
declare -r  UP="up"
declare -r  DOWN="down"
declare -ir ADJUSTMENT=3
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#
# NB only create OSD if brightness change succeeds.
if [[ ${DIRECTION} = ${UP} ]]; then
    fnUp && fnNotify
elif [[ ${DIRECTION} = ${DOWN} ]]; then
    fnDown && fnNotify
fi
