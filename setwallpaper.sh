#!/usr/bin/env bash
# ======================================================================================
#          FILE: setwallpaper.sh
# ======================================================================================
#   DESCRIPTION: Ensures that the background images (wallpaper) for the desktop, logon
#                screen and refind are kept in step.
#                Uses 'inotifywait' to monitor a key file which is updated when the
#                wallpaper changes and, when it does, uses 'gsettings' to get the path
#                to the current background image file and copy it to locations where it
#                will also be used as the wallpaper for the refind menu and logon screen.
# ======================================================================================
#         USAGE: setwallpaper
#         NEEDS: logger (linux-utils), convert (imagemagick), wallchanger
#     COPYRIGHT: Keith Watson (swillber@gmail.com)
#       VERSION: 3.1.1
#  DATE CREATED: ???
#  LAST CHANGED: 16-Nov-2024 21:35
#    KNOWN BUGS: ---
#          TODO: ---
# ======================================================================================
#  LICENSE:
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or (at my option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with this
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
#  Fifth Floor, Boston, MA 02110-1301, USA.
# ======================================================================================
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnCopyBGs () {
# ------------------------------------------------------------------------------------ #
# Copies the current desktop background image to the refind theme and login theme      #
# menus.                                                                               #
#                                                                                      #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  REFINDWALLPAPER="/boot/EFI/BOOT/images/background/background.png"
    declare -r  DMBACKGROUND="/usr/share/backgrounds/lxdmbg.png"
    # logging message literals
    declare -r REFINDFILE="Refind Wallpaper = %s"
    declare -r DMFILE="DM Background = %s"
    declare -r CURRENTBG="New desktop background is %s"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    DConfWallpaper
    declare -i Len
    declare    MateWallpaper
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get current wallpaper path as serialized GVariant
    DConfWallpaper="$(gsettings get org.mate.background picture-filename)"
    # get length of path and strip off delimiters to get usable path
    Len=${#DConfWallpaper}
    MateWallpaper="${DConfWallpaper:1:((Len-2))}"
    # log current desktop background file
    fnEcho "$(printf "${CURRENTBG}" "${MateWallpaper}")"
    # copy current wallpaper file
    sudo magick ${MateWallpaper} -resize 1920x1080! ${DMBACKGROUND} && {
        fnEcho "$(printf "${DMFILE}" "${DMBACKGROUND}")"
        sudo magick ${MateWallpaper} -resize 1024x768! ${REFINDWALLPAPER} && {
            fnEcho "$(printf "${REFINDFILE}" "${REFINDWALLPAPER}")"
        }
    }
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnEcho () {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin and add a newline.                     #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}" $'\n'
}
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
# config file for the wallchanger utility that is updated when the wallpaper is changed
declare -r TRIGGER="/home/swillber/.config/wchanger/config.json"
declare -r SEPARATOR="$(printf '=%.0s' {1..80})"
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#
# log runtime messages to syslog tagged with script name (use journalctl to list)
exec 1> >(logger -s -t $(basename $0)) 2>&1
#
fnEcho "${SEPARATOR}"
# at startup copy desktop background image to the refind theme and login theme menus
fnCopyBGs
#
while true; do
    #
    fnEcho "${SEPARATOR}"
    fnEcho "Waiting for '${TRIGGER}' to be updated"
    # wait until wall changer updates the wallpaper
    inotifywait -qq --event modify "${TRIGGER}"
    # copy current desktop background image to the refind theme and login theme menu
    fnCopyBGs
done
