#!/bin/bash
# ==================================================================================== #
#         FILE: deploy2bin.sh                                                          #
# ==================================================================================== #
#  DESCRIPTION: Create a symbolic link in ~/bin from the executable specified by the   #
#               single argument. Strip off any language specific suffix i.e. '.sh',    #
#               '.py', etc.                                                            #
# ==================================================================================== #
#        USAGE: deploy2bin [filespec]                                                  #
#        NEEDS: notify-send                                                            #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.0.0                                                                  #
# DATE CREATED: 27-Dec-2019                                                            #
# LAST CHANGED: 05-May-2020 20:13
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2019 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
# ------------------------------------------------------------------------------------ #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  SCRIPTNAME="${0##*/}"           # base name of script
declare -r  FILESPEC=${1}
declare -r  BASENAME="${FILESPEC##*/}"
declare -r  EXTENSION="${FILESPEC##*.}"
declare -r  FILENAME="${BASENAME%.*}"
declare -r  SYMLINK="${HOME}/bin/${FILENAME}"
declare -r  LINKFOUND="Deployment failed: ${SYMLINK} already exists!"
declare -r  DEPLOYOK="${FILESPEC} deployed to ${SYMLINK}"
declare -r  DEPLOYFAIL="Deployment of ${FILESPEC} failed!!"
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i  Result=${FALSE}
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
if [[ -L "${SYMLINK}" ]]; then
    notify-send -a "${SCRIPTNAME}" -i dialog-error -t 10000 "${LINKFOUND}"
else
    if [[ -f "${FILESPEC}" ]]; then
        ln -s "${FILESPEC}" "${SYMLINK}"
        Result=${?}
        if [ ${Result} -eq $TRUE ]; then
            notify-send -a "${SCRIPTNAME}" -i dialog-information -t 10000 "${DEPLOYOK}"
        fi
    fi
    if [[ ${Result} -ne $TRUE ]]; then
            notify-send -a "${SCRIPTNAME}" -i dialog-warning -t 10000 "${DEPLOYFAIL}"
    fi
fi
exit ${Result}
