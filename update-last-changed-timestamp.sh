#!/bin/bash
# ==================================================================================== #
#         FILE: update-last-changed-timestamp.sh                                       #
# ==================================================================================== #
#  DESCRIPTION: Obtains the date modified for the files supplied as command line       #
#               arguments and for each one looks for a header comment string of        #
#               LAST CHANGED and updates the comment with a formatted date string.     #
#               Then resets the date modified back to what it was before the           #
#               update.                                                                #
#                                                                                      #
# ==================================================================================== #
#        USAGE: see fnUsage()                                                          #
#        NEEDS: ---                                                                    #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.1.0                                                                  #
# DATE CREATED: 06/04/2021                                                             #
# LAST CHANGED: 06-Nov-2022 23:53                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r APP="Update Last Changed Timestamp"
declare -r TITLE="Processing..."
declare -r ICON="dialog-warning"
declare -r TIME=1500
declare -ar TARGET=(${@})
declare -r  SEARCH="LAST CHANGED: "
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i Result=${TRUE}
declare    ModifiedTS=""
declare    LastchangedTS=""
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
for Target in "${TARGET[@]}"; do
    ModifiedTS="$(stat --printf="%y" ${Target})"
    LastchangedTS=$(date --date="${ModifiedTS}" +"%d-%b-%Y %R")
    sed -E -i "0,/${SEARCH}/{s/${SEARCH}.{17}/${SEARCH}${LastchangedTS}/}" "${Target}"
    {
        sudo date --set="${ModifiedTS}" && touch "${Target}"
    } >/dev/null 2>&1
    # notify-send -t ${TIME} -a "${APP}" "${TITLE}" "${Target} - ${LastchangedTS}" --icon="${ICON}"
done
sleep 2
exit ${Result}
