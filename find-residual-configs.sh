#!/bin/bash
# ======================================================================================
#          FILE: find-residual-configs.sh
# ======================================================================================
#   DESCRIPTION: Attempts to produce a list of candidate residual config files in the
#                user's home directory which can be considered for deletion.
# ======================================================================================
#         USAGE: find-residual-configs
#         NEEDS: yaourt, zenity
#     COPYRIGHT: Keith Watson (swillber@gmail.com)
#       VERSION: 1.0
#  DATE CREATED: 05-May-2018 21:54
#  LAST CHANGED: 08-May-2020 16:58
#    KNOWN BUGS: ---
#          TODO: Add list of false residuals (such as .xinitrc) which should be
#                removed from candidate list.
# ======================================================================================
#  LICENSE:
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or (at my option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with this
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
#  Fifth Floor, Boston, MA 02110-1301, USA.
# ======================================================================================
#       S e t u p
# ======================================================================================
#
# switch to home directory
#
pushd ${HOME} > /dev/null 2>&1
#
# create temporary files in /tmp (in arch linux /tmp uses tmpfs i.e. RAM)
#
declare -r INSTALLED_PACKAGES=$(mktemp --tmpdir=/tmp Installed_Packages_XXXXXXXX.tmp)
declare -r DOTFILES=$(mktemp --tmpdir=/tmp Dotfiles_XXXXXXXX.tmp)
declare -r RESIDUAL_DOTFILES=$(mktemp --tmpdir=/tmp Residual_Dotfiles_XXXXXXXX.tmp)
declare -r INSTALLED_DOTFILES=$(mktemp --tmpdir=/tmp Installed_Dotfiles_XXXXXXXX.tmp)
declare -r CONFILES=$(mktemp --tmpdir=/tmp Confiles_XXXXXXXX.tmp)
declare -r RESIDUAL_CONFILES=$(mktemp --tmpdir=/tmp Residual_Confiles_XXXXXXXX.tmp)
declare -r INSTALLED_CONFILES=$(mktemp --tmpdir=/tmp Installed_Confiles_XXXXXXXX.tmp)
#
# get list of installed packages
#
yaourt -Qqe | sort > "${INSTALLED_PACKAGES}"
#
# constants for zentity progress display
#
declare -r  ZTITLE="Find Residual Configs"
declare -ir ZWIDTH=500
#
# zenity progress display variables
#
declare  Lines
declare  Count
declare  Percent
#
# ======================================================================================
#       M a i n   P r o c e s s i n g
# ======================================================================================
#
# get list of dotfiles (files and directories) in home
#
ls -a | grep "^\." | sed "s/^\.//" > "${DOTFILES}"
#
# get list of files and directories in .config directory
#
ls -a1 .config > "${CONFILES}"
#
# delete any blank lines, lines with just '.' or '..'
#
sed -i '/^$/d' "${DOTFILES}"
sed -i '/^$/d' "${CONFILES}"
sed -i '/^\.$/d' "${DOTFILES}"
sed -i '/^\.$/d' "${CONFILES}"
sed -i '/^\.\.$/d' "${DOTFILES}"
sed -i '/^\.\.$/d' "${CONFILES}"
#
# get dotfiles and config files lists into alphabetical order
#
sort -fo "${DOTFILES}" "${DOTFILES}"
sort -fo "${CONFILES}" "${CONFILES}"
#
# remove any exact matches by comparing installed package names with dotfiles
# and also compare installed packages with config files
#
comm -13 "${INSTALLED_PACKAGES}" "${DOTFILES}" > "${RESIDUAL_DOTFILES}"
comm -13 "${INSTALLED_PACKAGES}" "${CONFILES}" > "${RESIDUAL_CONFILES}"
#
# delete any residual lines that contain an installed package name (case insensitive)
#
while read Installed; do
    sed -i "/${Installed}/Id" "${RESIDUAL_DOTFILES}"
    sed -i "/${Installed}/Id" "${RESIDUAL_CONFILES}"
done < "${INSTALLED_PACKAGES}"
#
# look for and remove 'fuzzy' matches where residual names occur anywhere in the package
# descriptions.
#
Lines=$(printf "%s\n" "scale=0; $(wc -l < ${RESIDUAL_DOTFILES})+$(wc -l < ${RESIDUAL_CONFILES})" | bc)
#
(
    #
    # get list of residual dotfiles that match part of any of the installed package names
    while read Residual; do
        (( Count++ ))
        if grep -i "${Residual}" "${INSTALLED_PACKAGES}" > /dev/null 2>&1; then
            echo "${Residual}" >> "${INSTALLED_DOTFILES}"
        fi
        # add any dotfiles that give positive hits from a package manager search
        if (yaourt -Ss "${Residual}" | grep install >/dev/null 2>&1);then
            echo "${Residual}" >> "${INSTALLED_DOTFILES}"
        fi
        # calculate percentage progress
        Percent="$(printf "%s\n" "scale=2; ${Count}*100/${Lines}" | bc)"
        # output percentage to stdout as input to zenity
        printf "# Processing Residual %d of %d Residuals (%5.2f%%)\n%5.2f\n" \
               ${Count} ${Lines} ${Percent} ${Percent}
    done < "${RESIDUAL_DOTFILES}"
    #
    # get list of configs that match part of any of the installed package names
    while read Residual; do
        (( Count++ ))
        if grep -i "${Residual}" "${INSTALLED_PACKAGES}" > /dev/null 2>&1; then
            echo "${Residual}" >> "${INSTALLED_CONFILES}"
        fi
        # add any configs that give positive hits from a package manager search
        if (yaourt -Ss "${Residual}" | grep install >/dev/null 2>&1);then
            echo "${Residual}" >> "${INSTALLED_CONFILES}"
        fi
        # calculate percentage progress
        Percent="$(printf "%s\n" "scale=2; ${Count}*100/${Lines}" | bc)"
        # output percentage to stdout as input to zenity
        printf "# Processing Residual %d of %d Residuals (%5.2f%%)\n%5.2f\n" \
               ${Count} ${Lines} ${Percent} ${Percent}
    done < "${RESIDUAL_CONFILES}"
    # output final percentage to stdout as input to close zenity
    Percent="100.0000"
    printf "# Processing Residual %d of %d Residuals (%5.2f%%)\n%5.2f\n" \
           ${Count} ${Lines} ${Percent} ${Percent}
) |
    zenity --progress \
      --title="${ZTITLE}" \
      --width=${ZWIDTH} \
      --text="0%" \
      --percentage=0 \
      --time-remaining \
      --no-cancel \
      --auto-close \
      --auto-kill >/dev/null 2>&1
#
# ensure both lists are in alphabetical order
#
sort -fo "${INSTALLED_DOTFILES}" "${INSTALLED_DOTFILES}"
sort -fo "${INSTALLED_CONFILES}" "${INSTALLED_CONFILES}"
#
# delete the installed entries from the residual dotfiles list
#
while read Installed; do
    sed -i "/${Installed}/Id" "${RESIDUAL_DOTFILES}"
done < "${INSTALLED_DOTFILES}"
#
# delete the hits from the residual config list
#
while read Installed; do
    sed -i "/${Installed}/Id" "${RESIDUAL_CONFILES}"
done < "${INSTALLED_CONFILES}"
#
# ======================================================================================
#       F i n a l   H o u s e k e e p i n g
# ======================================================================================
#
# display results
#
while read Dotfile; do
    printf ".%s\n" "${Dotfile}"
done < "${RESIDUAL_DOTFILES}"
#
while read Confile; do
    printf ".config\\%s\n" "${Confile}"
done < "${RESIDUAL_CONFILES}"
#
# tidy up
#
rm "${INSTALLED_PACKAGES}"
rm "${DOTFILES}"
rm "${CONFILES}"
rm "${RESIDUAL_DOTFILES}"
rm "${RESIDUAL_CONFILES}"
rm "${INSTALLED_DOTFILES}"
rm "${INSTALLED_CONFILES}"
#
# switch back to original pwd
#
popd > /dev/null 2>&1
