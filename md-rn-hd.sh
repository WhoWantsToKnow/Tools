#!/bin/bash
# ==================================================================================== #
#         FILE: md-rn-hd.sh                                                            #
# ==================================================================================== #
#  DESCRIPTION: Process markdown (github) headers adding sequential numbering and      #
#               writing output to new file.                                            #
# ==================================================================================== #
#        USAGE: see fnUsage                                                            #
#        NEEDS:                                                                        #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 0.2                                                                    #
# DATE CREATED: 26-Jul-2017                                                            #
# LAST CHANGED: 09-Aug-2017                                                            #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2016 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnUsage () {
# ------------------------------------------------------------------------------------ #
# Display usage message.                                                               #
# Arguments: none.                                                                     #
# Returns: none.                                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r NL=$'\n'
    declare -r USAGE=$(cat <<EOSL

Usage:
    ${SCRIPTNAME} -[i|I] InFile -[o|O] OutFile [-[h|H]]
    Process markdown (github) headers adding sequential numbering and writing output to
    new file.
    where;
            InFile  = input filepath (M)
            OutFile = output filepath (M)
            -h|H    = displays this help text.
    NOTES:
        1) options are not case sensitive, uppercase will be converted to lowercase.
        2) (M) indicates mandatory option.

EOSL
)
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${USAGE}${NL}${NL}"
}
# ------------------------------------------------------------------------------------ #
function fnGetOpts () {
# ------------------------------------------------------------------------------------ #
# Get command line options (see fnUsage)                                               #
# Sets InFile  = command line option -i                                                #
#      OutFile = command line option -o                                                #
# Prints a usage message if any errors found.                                          #
# Arguments: All the command line options passed as $@.                                #
# Returns: true if options found ok otherwise false.                                   #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r OPTIONS=":i:I:o:O:hH"
    # --------------------------- Error Message Templates ---------------------------- #
    declare -r ERR1="ERROR: Missing argument: Option '-%s', must have an argument.\n"
    declare -r ERR2="ERROR: Not recognized: Option '-%s' is unknown.\n"
    declare -r ERR3="ERROR: %s is not a valid file or path.\n"
    declare -r ERR4="ERROR: %s already exists. Will not overwrite.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare Char1=""
    declare Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if [[ ${#} -gt 0 ]]; then
        while getopts "${OPTIONS}" OPT; do
            OPT="${OPT,,}"                  # ensure lowercase
            Char1="${OPTARG:0:1}"           # in case of a missing argument to an option
            if [[ ${Char1} == "-" ]]; then
                printf "${ERR1}" "${OPT}"
                Result=${FALSE}
                break
            fi
            case ${OPT} in
                i)  # input filepath
                    if [[ -f ${OPTARG} ]]; then
                        InFile="${OPTARG}"
                    else
                        printf "${ERR3}" "${OPTARG}"
                        Result=${FALSE}
                        break
                    fi
                    ;;
                o)  # output filepath
                    if [[ -e ${OPTARG} ]]; then
                        printf "${ERR4}" "${OPTARG}"
                        Result=${FALSE}
                        break
                    else
                        OutFile="${OPTARG}"
                    fi
                    ;;
                h)  # display help text
                    Result=${FALSE}
                    break
                    ;;
                :)  # missing argument to option
                    printf "${ERR1}" "${OPTARG}"
                    Result=${FALSE}
                    break
                    ;;
                \?) # invalid option
                    printf "${ERR2}" "${OPTARG}"
                    Result=${FALSE}
                    break
                    ;;
            esac
        done
    else
        Result=${FALSE}
    fi
    return "${Result}"
}
# ------------------------------------------------------------------------------------ #
function fnOutline () {
# ------------------------------------------------------------------------------------ #
# Compares the current header format string to the previous header format and sets     #
# the values of the Levels array accordingly.                                          #
# Arguments: 1 - current header string                                                 #
#            2 - previous header string                                                #
# Returns: none.                                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ir CURRLEVEL=${#CurrHead}
    declare -ir PREVLEVEL=${#PrevHead}
    declare -ir RESET=0
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i  Level
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if (( CURRLEVEL < PREVLEVEL )); then
        for (( i=CURRLEVEL; i<MAXHEAD; i++ )); do
            Levels[${i}]=${RESET}
            if (( i > MAXHEAD )); then
                break
            fi
        done
    fi
    (( Level = CURRLEVEL - 1 ))
    (( Levels[Level]++ ))
}
# ------------------------------------------------------------------------------------ #
function fnIsHeader () {
# ------------------------------------------------------------------------------------ #
# Scans the start of the current line looking for a string of consecutive hashes that  #
# signifies a header line. Uses the global variables 'InLine', 'MAXHEAD' and           #
# 'SKIPHEAD'.                                                                          #
# TODO: (Keith Watson 08-Aug-2017) Would a regex be a better option for this?          #
# Arguments: none                                                                      #
# Returns: True if a heading line is found, otherwise false.                           #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r HASH="#"
    declare -r ENDHEAD="# "
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Result=${FALSE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    for (( i=SKIPHEAD; i<MAXHEAD; i++ )); do
        if [[ ${InLine:${i}:1} != "${HASH}" ]]; then
            break                           # not a valid header format string
        elif [[ ${InLine:${i}:2} =~ ${ENDHEAD} ]]; then
            Result=${TRUE}                  # end of valid header format string
            break
        fi
    done
    return "${Result}"
}
# ------------------------------------------------------------------------------------ #
function fnProcess () {
# ------------------------------------------------------------------------------------ #
# Processes the current line (in InLine) inserting an outline number if a header       #
# format string is found at the beginning. Will overwrite any existing outline number  #
# or insert one if none. Skips code blocks delimited by three backticks. Sets OutLine  #
# to the processed InLine contents.                                                    #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r CODEBLKDELIM="\`\`\`*"
    declare -r NUMBER="[0-9]+(\.[0-9]+)*"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    HeadNumber                   # outline number string '99.99.99...'
    # ---------------------------------- PROCEDURE ----------------------------------- #
    OutLine="${InLine}"
    if (( CodeBlock == TRUE )); then        # currently processing code block
        if [[ ${InLine} == "$CODEBLKDELIM" ]]; then # end of code block
            CodeBlock=${FALSE}
        fi
    else                                    # not in code block
        if [[ ${InLine} == "$CODEBLKDELIM" ]]; then # start of code block
            CodeBlock=${TRUE}
        else
            if fnIsHeader; then             # line starts with header format string
                read -r -a Field <<< "${InLine}" # split line into fields
                PrevHead="${CurrHead}"      # remember previous format string
                CurrHead="${Field[0]}"      # first field is header format string
                fnOutline                   # update Levels array to reflect latest
                                            # header format string
                HeadNumber=""
                for Level in "${Levels[@]}"; do # build outline number string
                    if [[ ${Level} -gt 0 ]]; then
                        HeadNumber="${HeadNumber}"$(printf "%u." "${Level}")
                    fi
                done
                # either overwrite or insert outline number string
                if [[ ${Field[1]} =~ ${NUMBER} ]]; then
                    Field[1]="${HeadNumber}"
                else
                    Field[0]="${Field[0]} ${HeadNumber}"
                fi
                printf -v OutLine "%s " "${Field[@]}"
            fi
        fi
    fi
}
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  SCRIPTNAME="${0##*/}"           # base name of script
declare -r  MAXHEAD=5                       # number of header levels to track
declare -r  SKIPHEAD=1                      # number of header levels to ignore
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare     InLine
declare     OutLine                         # processed InLine
declare -ia Levels                          # used to track outline levels
declare     CurrHead=""                     # current header format string
declare     PrevHead=""                     # remember previous header format string
# ------------------------------- set by fnGetOpts() --------------------------------- #
declare     InFile
declare     OutFile
# ------------------------------------------------------------------------------------ #
declare -i  Result=${TRUE}
declare -i  CodeBlock=${FALSE}              # used by fnProcess to track code blocks
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
if fnGetOpts "${@}"; then                   # process command line arguments
    while IFS= read -r InLine; do           # read a line from InFile, preserve spaces
        fnProcess                           # process input line adding or updating
                                            # outline numbers in header lines
        printf "%s\n" "${OutLine}" >> "${OutFile}"
    done <"${InFile}"
else                                        # invalid command line arguments
    fnUsage
    Result=${FALSE}
fi
exit ${Result}
