#!/bin/bash
# ==================================================================================== #
#         FILE: resizewin.sh                                                           #
# ==================================================================================== #
#  DESCRIPTION: resize the active window as specified by the command line arguments.   #
#                                                                                      #
# ==================================================================================== #
#        USAGE: see usage comment                                                      #
#        NEEDS: xdpyinfo, wmctrl                                                       #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.0.0                                                                  #
# DATE CREATED: 12/01/2022                                                             #
# LAST CHANGED: ---                                                                    #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                      U S A G E                                       #
# ==================================================================================== #
#                                                                                      #
# usage: resizewin.sh g,x,y,w,h                                                        #
#                                                                                      #
# All five arguments are integers. The first value, g, is the gravity of the window,   #
# with 0 being the most common value (the default value for the window). Please see    #
# the EWMH specification for other values. The four remaining values are a standard    #
# geometry specification: x,y is the position of the top left corner of the window,    #
# and w,h is the width and height  of the window, with the exception that the value    #
# of -1 in any position is interpreted to mean that the current geometry value should  #
# not be modified.                                                                     #
#                                                                                      #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  NL=$'\n'
declare -r  SCRIPTNAME="${0##*/}"          # base name of script
declare -r  RUNDATE="$(date +%Y%m%d)"
declare -r  RUNDATIME="${RUNDATE}$(date +%H%M)"
declare -r  WINMANAGER="wmctrl"
declare -r  FIND="-r"
declare -r  RESIZE="-e"
declare -r  STATE="-b"
declare -r  ACTIVEWINDOW=":ACTIVE:"
declare -r  NOFULLSCREEN="remove,fullscreen"
declare -r  UNMAXIMISE="remove,maximized_vert,maximized_horz"
# get info about size of current display
declare -r  DIMENSIONS="$(cut -d' ' -f7 < <(xdpyinfo | grep 'dimensions:'))"
declare -ir WIDTH=${DIMENSIONS%x*}      # extract width of display
declare -ir HEIGHT=${DIMENSIONS#*x}     # extract height of display
# retrieve arguments set defaults where required
declare -r  GRAVITY=${1:-0}
declare -r  TOP=${2:-0}
declare -r  LEFT=${3:-0}
declare -r  RIGHT=${4:-${WIDTH}}
declare -r  BOTTOM=${5:-${HEIGHT}}
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i Result=${TRUE}
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
# if window is fullscreen, restore it to unmaximised
${WINMANAGER} ${FIND} ${ACTIVEWINDOW} ${STATE} ${NOFULLSCREEN}
${WINMANAGER} ${FIND} ${ACTIVEWINDOW} ${STATE} ${UNMAXIMISE}
# now set new window parameters
${WINMANAGER} ${FIND} ${ACTIVEWINDOW} ${RESIZE} ${GRAVITY},${TOP},${LEFT},${RIGHT},${BOTTOM}
exit ${Result}
