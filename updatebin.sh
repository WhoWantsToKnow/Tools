#!/bin/bash
# ======================================================================================
#          FILE: updatebin.sh
# ======================================================================================
#   DESCRIPTION: Copy bash scripts to a target directory stripping off ".sh" extension.
# ======================================================================================
#         USAGE: updatebin
#         NEEDS: ---
#     COPYRIGHT: Keith Watson (swillber@gmail.com)
#       VERSION: 1.0.4
#  DATE CREATED: 16-May-2020 22:46
#  LAST CHANGED: 25-Jun-2020 13:03
#    KNOWN BUGS: ---
# ======================================================================================
#  LICENSE:
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or (at my option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with this
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
#  Fifth Floor, Boston, MA 02110-1301, USA.
# ======================================================================================
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnEcho {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ------------------------------------------------------------------------------------ #
function fnCopy {
# ------------------------------------------------------------------------------------ #
# given a directory name, clone the corresponding git project and deploy all the       #
# downloaded scripts to the local bin directory. Afterwards remove the cloned files    #
# and directory.                                                                       #
# Arguments:                                                                           #
#   1) git project (directory) to be cloned (downloaded)                               #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    declare -r DIR="${1}"
    declare -r PROJECT="https://gitlab.com/WhoWantsToKnow/${DIR}.git"
    declare -r STARTMSG="\nDeploying script files from ~/${DIR} to ~/${TARGET}\n"
    declare -r RMMSG="Removing any existing ~/${DIR} directory and contents${ELPS}"
    declare -r RMWRK="Removing working ~/${DIR} directory and contents${ELPS}"
    declare -r GITMSG="Downloading git project to ~/${DIR}${ELPS}"
    declare -r MVMSG="Moving scripts to ${TARGET} and removing '.sh' extension${ELPS}\n"
    declare -r OKMSG="${ELPS}success.\n"
    declare -r FAIL="${ELPS}failure.\n"
    declare -r ABORT="Execution terminated.\n\n"
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "${STARTMSG}"
    fnEcho "${RMMSG}"
    if rm -rf "${DIR}"; then
        fnEcho "${OKMSG}"
    else
        fnEcho "${FAIL}"
        fnEcho "${ABORT}"
        exit 999
    fi
    #
    fnEcho "${GITMSG}"
    if git clone --quiet "${PROJECT}"; then
        fnEcho "${OKMSG}"
    else
        fnEcho "${FAIL}"
        fnEcho "${ABORT}"
        exit 999
    fi
    #
    fnEcho "${MVMSG}"
    for File in ${DIR}/*.sh; do
        Filename="${File##*/}"
        if ! mv -vf "${File}" "${TARGET}/${Filename%%.*}"; then
            fnEcho "${FAIL}"
            fnEcho "${ABORT}"
            exit 999
        fi
    done
    #
    fnEcho "${RMWRK}"
    if rm -rf "${DIR}"; then
        fnEcho "${OKMSG}"
    else
        fnEcho "${FAIL}"
        fnEcho "${ABORT}"
        exit 999
    fi
}
# ------------------------------------------------------------------------------------ #
#                                      M A I N                                         #
# ==================================================================================== #
#                                                                                      #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -r ELPS=$(printf "\\u2026")    # ellipsis (…) character
declare -r TARGET="${1:-bin}"
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
    # save the current working directory and return the the home root
    pushd ${HOME} >/dev/null 2>&1
    #
    fnCopy "Tools"
    #
    fnCopy "Backup"
    # return to the original working directory
    popd >/dev/null 2>&1
    fnEcho "\n"
exit
