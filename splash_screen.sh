#!/bin/bash
# ==================================================================================== #
#         FILE: splash_screen.sh                                                       #
# ==================================================================================== #
#  DESCRIPTION: Using a supplied splash screen image, this script will display that    #
#               image in a borderless window using feh. The image will be centred on   #
#               the desktop background. There is also the option to add header and     #
#               footer text above and below the image.                                 #
#               Changes the class name of the window to "Splash" so that other         #
#               processes can identify it and returns the window handle of the         #
#               displayed splash screen.                                               #
# ==================================================================================== #
#        USAGE: splash_screen arg1 arg2 arg3 arg4 arg5 arg6                            #
#               arg1 = full path to the splash screen image                            #
#               arg2 = text to display above the splash image - header                 #
#               arg3 = pointsize for header text                                       #
#               arg4 = text to display below the splash image - footer                 #
#               arg5 = pointsize for footer text                                       #
#               arg6 = no. seconds to wait before closing splash screen                #
#                      0=don't close if not given this is the default                  #
#               NB no validation of supplied arguments                                 #
#        NEEDS: feh xdpyinfo identify(from ImageMagick)                                #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 3.0.2                                                                  #
# DATE CREATED: 05-May-2020 20:05                                                      #
# LAST CHANGED: 24-Dec-2021 20:36                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2020 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnEcho {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
#
# set parameters for script
declare -r  IMAGE="${1}"                 # path to folder containing splash image files
declare -r  HEADER="${2}"                # text to display above the splash image
declare -ir HSIZE=${3}                   # pointsize for header text
declare -r  FOOTER="${4}"                # text to display below the splash image
declare -ir FSIZE=${5}                   # pointsize for footer text
declare -r  TIMEOUT=${6:-0}              # timeout value (default 0)
# get dimensions of desktop display
declare -r DIMENSIONS=$(xdpyinfo | grep dimensions | cut -d' '  -f7)
declare -r DWIDTH=$(echo $DIMENSIONS | cut -dx -f1)
declare -r DHEIGHT=$(echo $DIMENSIONS | cut -dx -f2)
#                                                                                      #
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
#
# dimensions of splash screen image + text
declare -i WIDTH=0
declare -i HEIGHT=0
# offsets to centre splash image
declare -i XOFF=0
declare -i YOFF=0
# holds the window handle of the displayed splash screen
declare -i WINHAND=0
# counter for loop waiting for return of splash screen window handle
declare -i WHC=0
declare -i MAX=25  # maximum number of loop iterations allowed
# define a work file name to hold annotated splash screen
declare    SPLASH=""
#
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#
# generate new workfile name
SPLASH=$(mktemp -p /tmp splash-work-XXXXXXXX.png)
# make sure it's deleted after script finishes
trap "rm -f ${SPLASH}" EXIT
#
# build the splash screen image with any supplied header and footer
convert                                    \
       -background Black                   \
       -fill CornflowerBlue                \
       -font Verdana                       \
       -pointsize ${HSIZE}                 \
       -gravity center                     \
        label:"${HEADER}"                  \
        ${IMAGE}                           \
       -background Black                   \
       -fill CornflowerBlue                \
       -font Verdana-Italic                \
       -pointsize ${FSIZE}                 \
       -gravity center                     \
        label:"${FOOTER}"                  \
       -append                             \
       "${SPLASH}"
# get dimensions of splash screen image
WIDTH=$(identify -format '%w' ${SPLASH})
HEIGHT=$(identify -format '%h' ${SPLASH})
# calculate offsets to centre splash screen image
XOFF=$(( (DWIDTH - WIDTH)/2 ))
YOFF=$(( (DHEIGHT - HEIGHT)/2 ))
# display the splash screen image
if (( TIMEOUT == 0 )); then
    # display image without any scheduled closure
    feh -.xg "+${XOFF}+${YOFF}" --on-last-slide hold "${SPLASH}" >/dev/null 2>&1 &
else
    # display image and close after $TIMEOUT seconds
    feh -.xg "+${XOFF}+${YOFF}" -D ${TIMEOUT} --on-last-slide quit "${SPLASH}" >/dev/null 2>&1 &
fi
# wait until splash screen appears then, get its window handle
until (( WINHAND > 0 )); do
    WINHAND=$(xdotool search --sync --onlyvisible --name ".*${SPLASH}.*")
    (( WHC++ ))
    if (( WHC > MAX )); then
        break
    fi
done
# remove from pager and taskbar and raise
wmctrl -i -r ${WINHAND} -b add,skip_taskbar,skip_pager
wmctrl -i -r ${WINHAND} -b add,modal,above
wmctrl -i -R ${WINHAND}
# change the classname so it can be easily identified by other processes
xdotool set_window --classname "Splash" ${WINHAND}
# return the splash screens window handle
fnEcho "${WINHAND}"
