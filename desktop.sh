#!/bin/bash
# ======================================================================================
#          FILE: desktop.sh
# ======================================================================================
#   DESCRIPTION: Move the display focus to either the next desktop to the right or the
#                previous desktop to the left. Takes one argument, a string either
#                'Prev' or 'Next' (case insensitive), default is 'Next'.
# ======================================================================================
#         USAGE: desktop [Prev|Next]
#         NEEDS: wmctrl,xdpyinfo
#     COPYRIGHT: Keith Watson (swillber@gmail.com)
#       VERSION: 1.0.2
#  DATE CREATED: 22-Dec-2021 19:50
#  LAST CHANGED: 12-Jan-2022 15:04
#    KNOWN BUGS: ---
# ======================================================================================
#  LICENSE:
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or (at my option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with this
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
#  Fifth Floor, Boston, MA 02110-1301, USA.
# ======================================================================================
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
# notify-send
declare -r  DIMENSIONS="$(cut -d' ' -f7 < <(xdpyinfo | grep 'dimensions:'))"
declare -ir WIDTH=${DIMENSIONS%x*}
declare -ir HEIGHT=${DIMENSIONS#*x}
declare -ir EXPIRE=500
#                                                                                      #
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare    Direction
declare -a DeskTop
declare -i MaxDesk=0
declare -i DeskNo=99
# notify-send
declare    Summary=""
declare    BodyText=""
declare    X=$(( WIDTH/2 - 200 ))
declare    Y=$(( HEIGHT/2 - 150 ))
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
IFS=$'\n'                               # set field separator to newline only
#
# Get all desktops managed by the window manager, a line for each with space separated
# columns.
#         Col                                Content
#         ---  -----------------------------------------------------------------------
#          1   integer desktop number (0 based)
#          2   '*' for current desktop, else '-'
#         3/4  'DG:' then desktop geometry as '<width>x<height>' (e.g. '1280x1024')
#         5/6  'VP:' then viewport position as '<y>,<y>' (e.g. '0,0')
#         7-9  'WA:' then the workarea geometry as 'X,Y and WxH' (e.g. '0,0 1280x998')
#         10.. the name of the desktop (possibly containing multiple spaces)
#
Direction="${1}"                        # get the first argument
Direction="${Direction,,}"              # convert to lower case
case "${Direction}" in                  # validate argument
    prev)
        true
        ;;
    next)
        true
        ;;
    *)
        notify-send -i "error" "ERROR" "Argument must be 'Prev' or 'Next'(case insensitive)."
        exit 1
        ;;
esac
DeskTop=($(wmctrl -d))                 # get desktop data as an array
# get current desktop number
DeskNo=99                              # 99=not found
for D in "${DeskTop[@]}";do
    if [[ "${D:3:1}" == "*" ]];then
        DeskNo=${D:0:1}
        break
    else
        continue
    fi
done
MaxDesk=${#DeskTop[@]}                 # get number of desktops
# switch desktop
if [[ "${DeskNo}" == "99" ]]; then
    # not found so display error message
    notify-send -i "error" "ERROR" "Current desktop not found."
else
    # depending on direction calculate new desk number
    if [[ "${Direction}" == "next" ]]; then
        DeskNo=$(( (DeskNo+1)%MaxDesk ))
        BodyText="\t\t\t<b>===></b>"
    else
        DeskNo=$(( (DeskNo+MaxDesk-1)%MaxDesk ))
        BodyText="\t\t\t<b>&lt;===</b>"
    fi
    # set up notification arguments
    Summary="   Switched to desktop #$(( DeskNo+1 ))"
    Icon="/home/swillber/Pictures/Images/Workspace$(( DeskNo+1 )).jpg"
    wmctrl -s ${DeskNo}
    notify-send.sh --icon="${Icon}" --expire-time=${EXPIRE} --hint="int:x:${X}" \
                   --hint="int:y:${Y}" "${Summary}" "${BodyText}"
fi
exit
