#!/bin/sh
#
# Find and replace by a given list of files.
# https://github.com/thoughtbot/dotfiles/blob/master/bin/replace
#
# replace foo bar **/*.rb
# set -vx
#
scriptname=$(basename $0)
#
if [[ $# -lt 3 ]];then
    echo "usage: ${scriptname} old new filespec"
    exit 1
fi
#
find_this="$1"
shift
replace_with="$1"
shift
#
items=($(grep -l -E --color=never "$find_this" "$@"))
#
if [[ ${#items[@]} -lt 1 ]];then
    echo "${scriptname}: no files found matching \"${find_this}\""
    exit 1
fi
#
echo "${scriptname} found "${#items[@]}" file(s) matching \"${find_this}\""
#
IFS=$'\n'
for item in "${items[@]}"; do
  if $(sed -ri "s/$find_this/$replace_with/g" "$item"); then
    echo "    ${item}: \"${find_this}\" replaced with \"${replace_with}\""
  fi
done
