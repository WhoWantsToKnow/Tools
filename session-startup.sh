#!/bin/bash
# ==================================================================================== #
#         FILE: session-startup.sh                                                     #
# ==================================================================================== #
#  DESCRIPTION: script to be run at LXQt session start to initiate a series of         #
#               startup tasks and also display an associated splash screen for each.   #
#               Simple progress messages are output to a log file for the display of   #
#               each splash screen. At startup the previous log file is added to a     #
#               compressed archive.                                                    #
# ==================================================================================== #
#        USAGE: session-startup                                                        #
#        NEEDS: splash_screen xdotool [configuration file]                             #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 2.2.2                                                                  #
# DATE CREATED: 09-May-2020 21:32                                                      #
# LAST CHANGED: 08-Mar-2021 19:07                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnEcho {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ------------------------------------------------------------------------------------ #
function fnTrim {
# ------------------------------------------------------------------------------------ #
# Strips leading and trailing whitespace and quotes from argument string and returns   #
# trimmed value.                                                                       #
# Code copied from...                                                                  #
#   https://stackoverflow.com/questions/369758/                                        #
#                          how-to-trim-whitespace-from-a-bash-variable                 #
# and..                                                                                #
#   https://stackoverflow.com/questions/9733338/                                       #
#                          shell-script-remove-first-and-last-quote-from-a-variable    #
# Arguments:                                                                           #
#   1) string to be trimmed                                                            #
# Returns:                                                                             #
#   trimmed string                                                                     #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    declare STRING="${*}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # remove leading whitespace characters
    STRING="${STRING#"${STRING%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    STRING="${STRING%"${STRING##*[![:space:]]}"}"
    # remove any leading quote character
    STRING="${STRING#\"}"
    # remove any trailing quote character
    STRING="${STRING%\"}"
    #
    fnEcho "${STRING}"
}
# ------------------------------------------------------------------------------------ #
function fnOrdinal {
# ------------------------------------------------------------------------------------ #
# Returns an ordinal suffix for a supplied cardinal number.                            #
# Arguments:                                                                           #
#   1) cardinal number for which ordinal suffix is to be returned.                     #
# Returns:                                                                             #
#   ordinal suffix                                                                     #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    declare -r LASTCHAR=${1:${#1}-1:1}  # get last character (digit)
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    case ${LASTCHAR} in
        1) fnEcho "st";;
        2) fnEcho "nd";;
        3) fnEcho "rd";;
        *) fnEcho "th";;
    esac
}
# ------------------------------------------------------------------------------------ #
function fnHeader {
# ------------------------------------------------------------------------------------ #
# Returns a header string with an updated date/time string.                            #
# Arguments:                                                                           #
#   none.                                                                              #
# Returns:                                                                             #
#   header string                                                                      #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    declare -r  OS="$(uname -o)"
    declare -r  REL="$(uname -r)"
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "${OS} ${REL}     $(date "+%a,%e$(fnOrdinal $(date +%d)) %b %Y %R%p")"
}
# ------------------------------------------------------------------------------------ #
function fnLoadConfig {
# ------------------------------------------------------------------------------------ #
# Reads the configuration file and loads the FOOTER and COMMAND arrays.                #
# NB Not original code but shamelessly filched from...                                 #
#   https://www.cyberciti.biz/faq/unix-linux-bash-read-comma-separated-cvsfile/        #
# and                                                                                  #
#   https://unix.stackexchange.com/questions/244465/                                   #
#              how-to-make-bash-built-in-read-ignore-commented-or-empty-lines          #
# Arguments:                                                                           #
#   none.                                                                              #
# Returns:                                                                             #
#   populated arrays                                                                   #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    declare    Footer=""
    declare    Command=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    while IFS=, read Footer Command; do
        # skip blank lines and lines starting with '#'
        case ${Footer} in
            ''|\#*) continue ;;
        esac
        # trim leading and trailing whitespace and quotes
        Footer=$(fnTrim "${Footer}")
        Command=$(fnTrim "${Command}")
        # append values to arrays
        FOOTER+=("${Footer}")
        COMMAND+=("${Command}")
    done < "${CONFIGFILE}"
}
# ------------------------------------------------------------------------------------ #
function fnSplash {
# ------------------------------------------------------------------------------------ #
# display the current splash screen image using the footer array defined via an index  #
# value supplied.                                                                      #
# Arguments:                                                                           #
#   1). index to arrays                                                                #
# Returns:                                                                             #
#   set SPLASHHANDLE to window id of displayed splash screen                               #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    declare -ir HSIZE=20
    declare -ir FSIZE=22
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    declare     INDEX=${1}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnLog "...${FOOTER[${INDEX}]}"
    SPLASHHANDLE=$(/home/swillber/bin/splash_screen     \
                             "${IMAGE}"                 \
                             "$(fnHeader)"              \
                             ${HSIZE}                   \
                             "${FOOTER[${INDEX}]}"      \
                             ${FSIZE} &)
}
# ------------------------------------------------------------------------------------ #
function fnKill {
# ------------------------------------------------------------------------------------ #
# Kills the window specified by the window handle value supplied.                      #
# Arguments:                                                                           #
#   1). window handle                                                                  #
# Returns:                                                                             #
#   none.                                                                              #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    declare -i SC=0
    declare    KILLOG=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    KILLOG=$(xdotool windowkill ${1} 2>&1) || SC=${?}
    if (( RC > 0 )); then
        fnLog "${KILLOG}"
    fi
}
# ------------------------------------------------------------------------------------ #
function fnLog {
# ------------------------------------------------------------------------------------ #
# Write the supplied arguments as a timestamped entry to a pre-defined log file.       #
# Arguments:                                                                           #
#   1-n). log message                                                                  #
# Returns:                                                                             #
#   none.                                                                              #
# ------------------------------------------------------------------------------------ #
    # ------------------------------- LOCAL CONSTANTS -------------------------------- #
    declare -r TIMESTAMP="$(date "+%d-%b-%Y %H:%M:%S.%2N")"
    # ------------------------------- LOCAL VARIABLES -------------------------------- #
    declare    LOGMSG="${TIMESTAMP} ${*}\n"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${LOGMSG}" >> "${LOGFILE}"
}
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
#
declare -r  SCRIPTNAME="$(basename ${0})"
declare -r  STARTIME=$(date +%y%m%d%H%M%S)
declare -r  LOGROOT="${HOME}/.log/${SCRIPTNAME}"
declare -r  LOGFILE="${LOGROOT}/${SCRIPTNAME}${STARTIME}.log"
declare -r  LOGARCHIVE="${LOGROOT}/${SCRIPTNAME}.zip"
declare -r  CONFIGFILE="${HOME}/.config/lxqt/${SCRIPTNAME}.conf"
declare -r  DELAY="0.25"
declare -r  FOLDER="/home/swillber/Pictures/Wallpapers/Arch/Splash"
declare -r  IMAGE="$(/usr/bin/shuf -n1 -e ${FOLDER}/*)"
declare -r  MEGA="/home/swillber/Clouds/Remote/MEGA/"
declare -r  GOOGLE="/home/swillber/Clouds/Remote/GoogleDrive/"
# ------- Logging messages --------
declare -r  LOGSEP="---------------------------------------------------------------------"
declare -r  LOGSPNAME="Display splash screen for..."
declare -r  LOGWINH="Window handle for splash screen was:"
declare -r  LOGSAVE="Saved splash screen window handle:"
declare -r  LOGKILL="Closing splash screen with window handle:"
declare -r  LOGEXEC="Invoking:"
#
#                                                                                      #
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
#                                                                                      #
# define array to hold messages to be displayed on the splash screen
declare -a FOOTER
# define an array to hold command strings to be invoked by the script
declare -a COMMAND
declare    LASTHANDLE="0"
declare    XDTOUT=""
declare -i RC=0
# splash screen window handle, return value of call to splash screen script
declare -i SPLASHHANDLE=0
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
#
# load FOOTER and COMMAND arrays from config file
fnLoadConfig
# move any previous logs to the log archive
  for Logfile in $LOGROOT/*.log; do
    /usr/bin/zip -9jm "${LOGARCHIVE}" "${Logfile}"
  done
# list archive to new log
#  while IFS= read -r LINE;do
#      fnLog "${LINE}"
#  done < <(/usr/bin/zipinfo "${LOGARCHIVE}")
#
for (( INDEX=0; INDEX<${#COMMAND[@]}; INDEX++ )) ; do
    fnLog "INDEX=${INDEX}..."
    # display splash screen
    fnSplash ${INDEX}
    # sets SPLASHHANDLE to window handle of displayed screen
    fnLog "${LOGWINH}${SPLASHHANDLE}"
    # pause so messages can be seen
    sleep ${DELAY}
    # if not 1st screen, kill previous instance
    if (( LASTHANDLE > 0 )); then
        fnLog "${LOGKILL}${LASTHANDLE}"
        fnKill ${LASTHANDLE}
    fi
    # save the window handle
    LASTHANDLE=${SPLASHHANDLE}
    fnLog "${LOGSAVE}${LASTHANDLE}"
    # execute associated commnd
    fnLog "${LOGEXEC}${COMMAND[${INDEX}]}"
    eval "${COMMAND[${INDEX}]}"
    fnLog "${LOGSEP}"
done
# close the last screen
fnLog "${LOGKILL}${LASTHANDLE}"
fnKill ${LASTHANDLE}
