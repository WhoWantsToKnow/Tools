
#!/bin/bash
# ==================================================================================== #
#         FILE: gitstatusall.sh                                                        #
# ==================================================================================== #
#  DESCRIPTION: starting from current directory find directories with a '.git' file    #
#               and generate a git status report for each repository found.            #
#                                                                                      #
# ==================================================================================== #
#        USAGE: see fnUsage()                                                          #
#        NEEDS: ---                                                                    #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.0.0                                                                  #
# DATE CREATED: 02/09/2024                                                             #
# LAST CHANGED: ---                                                                    #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  NL=$'\n'
declare -r  BOLD=$(tput bold)
declare -r  NORMAL=$(tput sgr0)
declare -r  SCRIPTNAME="${0##*/}"          # base name of script
declare -r  RUNDATE="$(date +%Y%m%d)"
declare -r  RUNDATIME="${RUNDATE}$(date +%H%M)"
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare    GitPath=""
declare -a GitPaths
declare    BaseName
declare -a GitStatus
declare    GitRoot
declare    Clean=${FALSE}
declare -i Result=${TRUE}
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
GitPaths=$(find . -type d -name ".git" -printf "%h ")
for GitPath in ${GitPaths[@]}; do
    pushd "${GitPath}" >/dev/null
    GitRoot="$(dirname "${GitPath}")"
    GitRoot="$(basename "${GitRoot}")"
    BaseName="${GitRoot}/$(basename "${GitPath}")"
    readarray -t GitStatus < <(git status)
    Clean=${FALSE}
    for Line in "${GitStatus[@]}"; do
        if [[ "${Line}" == *'nothing to commit, working tree clean'* ]]; then
            Clean=${TRUE}
            break
        fi
    done
    if (( Clean == FALSE )); then
        printf "\n${BOLD}%s${NORMAL}\n" "${BaseName}"
        for Line in "${GitStatus[@]}"; do
            Line="${Line/(*)/}"
            case ${Line} in
                'Your branch'*| \
                'nothing to commit'*| \
                'nothing added'*| \
                'no changes'*)
                    printf "%s\n" "${Line}"
                    ;;
                'On branch'*| \
                'Untracked'*| \
                'Changes not'*)
                    printf "%s, " "${Line}"
                    ;;
                *)
                    continue
                    ;;
            esac
        done
    fi
    popd >/dev/null
done
exit ${Result}
